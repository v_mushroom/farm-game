# Simple Farming Game

An attempt to build a simple 2D game from the ground up.
This does not mean I will not be using helpful utility libraries, like configuration readers etc., I just won't be using any actual existing game engines and the like.

The point of this project is personal fun, and a learning experience. 
The only goal so far is to get to a subjective point when the game is playable enough to be called a game, before I lose interest.

I will also when possible try to stream the development on Twitch, for the (questionable) benefit and (even more questionable) amusement of others.
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.farm.game.model;

import com.mush.farm.game.logic.ai.CreatureAi;
import com.mush.farm.game.GameEventQueue;
import com.mush.farm.game.events.CreatureEvent;
import java.awt.Point;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;

/**
 * MOB
 *
 * @author mush
 */
public class Creature {

    public final int creatureId;
    public final Body body;
    public final Point2D.Double movementDirection = new Point2D.Double(0, 0);
    private final double movementSpeed;

    private Body equippedBody;
    public final CreatureControl control;
    public CreatureAi creatureAi = null;
    public boolean aiEnabled = false;
    public final List<Point> collisionTilePoints;
    public Integer selectedBodyId;

    public Creature(int id, Body body) {
        this.creatureId = id;
        this.body = body;
        this.movementSpeed = 50;
        this.body.creature = this;
        this.control = new CreatureControl(this);
        this.collisionTilePoints = new ArrayList<>();
        BodyState.setInitialStates(this);
    }

    public void die() {
        if (creatureAi != null) {
            creatureAi.removeFromCreature();
        }
        body.creature = null;
        collisionTilePoints.clear();
    }

    public void update(double elapsedSeconds) {
        if (creatureAi != null && aiEnabled) {
            creatureAi.update(elapsedSeconds);
        }

        double xOffset = movementDirection.x * elapsedSeconds * movementSpeed;
        double yOffset = movementDirection.y * elapsedSeconds * movementSpeed;

        if (xOffset != 0 || yOffset != 0) {
            GameEventQueue.send(new CreatureEvent.Move(this, xOffset, yOffset));
        }
        
        checkReproduction(elapsedSeconds);
    }

    public void unSelect() {
        selectedBodyId = null;
    }

    public void select(Body body) {
        selectedBodyId = body.bodyId;
    }

    public boolean hasSelected() {
        return selectedBodyId != null;
    }

    public void cycleInventory() {
        Body inventoryItem = removeFromInventory(0);
        if (inventoryItem != null) {
            addToInventory(inventoryItem);
        }
    }

    public boolean equipFromInventory(int index) {
        if (canEquip()) {
            Body inventoryItem = removeFromInventory(index);
            Body equippedItem = unEquip();
            if (equippedItem != null) {
                addToInventory(equippedItem);
            }
            if (inventoryItem != null) {
                equip(inventoryItem);
            }
            return true;
        } else {
            return false;
        }
    }

    public void unequipIntoInventory() {
        Body equippedItem = unEquip();
        if (equippedItem != null) {
            addToInventory(equippedItem);
        }
    }

    public Body unequipDirectly() {
        return unEquip();
    }

    public boolean equipDirectly(Body item) {
        if (canEquip()) {
            equip(item);
            body.updateContainedBodiesPosition();
            return true;
        } else {
            return false;
        }
    }

    private boolean canEquip() {
        return BodyType.getCanEquip(body.type);
    }

    private void equip(Body item) {
        equippedBody = item;
    }

    private Body unEquip() {
        Body item = equippedBody;
        equippedBody = null;
        return item;
    }

    public boolean hasEquipped() {
        return equippedBody != null;
    }

    public Body getEquipped() {
        return equippedBody;
    }

    public void addToInventory(Body item) {
        body.containedBodies.add(item);
        body.updateContainedBodiesPosition();
    }

    public Body removeFromInventory(int index) {
        try {
            Body item = body.containedBodies.remove(index);
            return item;
        } catch (IndexOutOfBoundsException ex) {
            return null;
        }
    }

    public Body removeLastFromInventory() {
        if (body.containedBodies.isEmpty()) {
            return null;
        }
        return removeFromInventory(body.containedBodies.size() - 1);
    }

    public List<Body> getInventory() {
        return body.containedBodies;
    }
    
    private void checkReproduction(double time) {
        // TODO: check this only once, store in a final property
        int maxReproductions = BodyType.getCanReproduceTimes(body.type);
        if (maxReproductions == 0) {
            return;
        }
        double[] reproductionAge = BodyType.getCanReproduceAge(body.type);
        double agePercent = body.getAgePercent();
        if (agePercent < reproductionAge[0] || agePercent > reproductionAge[1]) {
            return;
        }

        Integer produced = (Integer) body.getState(BodyState.OFFSPRING_PRODUCED);
        if (produced >= maxReproductions) {
            return;
        }

        if (Math.random() < 0.2 * time) {
            GameEventQueue.send(new CreatureEvent.Reproduce(this));
            body.setState(BodyState.OFFSPRING_PRODUCED, produced + 1);
        }
    }    

}

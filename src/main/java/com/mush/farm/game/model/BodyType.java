/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.farm.game.model;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author mush
 */
public enum BodyType {
    PERSON,
    POTATO,
    SPOILED_POTATO,
    PLANT_WASTE,
    HOG,
    SKULL,
    BUCKET,
    SHOVEL,
    FLY;

    private static final Map<BodyType, Box> boundingBoxMap;
    private static final Map<BodyType, BodyType> decayMap;
    private static final Map<BodyType, Double> ageMap;
    private static final Map<BodyType, double[]> healParamsMap;
    private static final Map<BodyType, double[]> hurtParamsMap;
    private static final Map<BodyType, Boolean> canEquipMap;
    private static final Map<BodyType, Integer> canReproduceTimesMap;
    private static final Map<BodyType, double[]> canReproduceAgeMap;

    static {
        boundingBoxMap = new HashMap<>();
        decayMap = new HashMap<>();
        ageMap = new HashMap<>();
        healParamsMap = new HashMap<>();
        hurtParamsMap = new HashMap<>();
        canEquipMap = new HashMap<>();
        canReproduceTimesMap = new HashMap<>();
        canReproduceAgeMap = new HashMap<>();

        setBoundingBox(PERSON, 7, 8, 7, 0);
        setBoundingBox(HOG, 7, 8, 7, 0);

        setMaxAge(PERSON, 120);
        setMaxAge(HOG, 10);
        setMaxAge(POTATO, 10);
        setMaxAge(SPOILED_POTATO, 120);

        setDecaysTo(POTATO, SPOILED_POTATO);
        setDecaysTo(SPOILED_POTATO, PLANT_WASTE);
        setDecaysTo(PERSON, SKULL);
        setDecaysTo(HOG, SKULL);

        setHealParams(PERSON, 1, 0.5, 0.1);
        setHurtParams(PERSON, 1, 0.5, 1);
        
        setCanEquip(PERSON);
        
        setCanReproduce(HOG, 1, 0.34, 0.8);
    }
    
    private static void setBoundingBox(BodyType type, int left, int top, int right, int bottom) {
        Box box = new Box(left, top, right, bottom);
        boundingBoxMap.put(type, box);
    }

    private static void setMaxAge(BodyType type, double age) {
        ageMap.put(type, age);
    }

    private static void setDecaysTo(BodyType type, BodyType toType) {
        decayMap.put(type, toType);
    }

    public static void setBoundingBox(Body body) {
        Box box = boundingBoxMap.get(body.type);
        body.setBoundingBox(box);
    }

    public static Double getMaxAge(BodyType type) {
        Double age = ageMap.get(type);
        return age != null ? age : Double.POSITIVE_INFINITY;
    }

    public static BodyType getDecaysTo(BodyType type) {
        BodyType toType = decayMap.get(type);
        return toType != null ? toType : type;
    }

    private static void setHealParams(BodyType type, double... values) {
        healParamsMap.put(type, values);
    }

    private static void setHurtParams(BodyType type, double... values) {
        hurtParamsMap.put(type, values);
    }
    
    private static void setCanEquip(BodyType type) {
        canEquipMap.put(type, Boolean.TRUE);
    }

    private static void setCanReproduce(BodyType type, int times, double ageBegin, double ageEnd) {
        canReproduceTimesMap.put(type, times);
        canReproduceAgeMap.put(type, new double[]{ageBegin, ageEnd});
    }
    
    public static double[] getHealParams(BodyType type) {
        return healParamsMap.get(type);
    }

    public static double[] getHurtParams(BodyType type) {
        return hurtParamsMap.get(type);
    }
    
    public static boolean getCanEquip(BodyType type) {
        Boolean value = canEquipMap.get(type);
        return value != null ? value : false;
    }
    
    public static int getCanReproduceTimes(BodyType type) {
        Integer times = canReproduceTimesMap.get(type);
        return times != null ? times : 0;
    }
    
    public static double[] getCanReproduceAge(BodyType type) {
        return canReproduceAgeMap.get(type);
    }

//    heal factor on 0..1 x age, 
//    hurt factor        
    // setHeal(1, 0.5, 0.1) -> 0:1, 0.5:0.5, 1:0.1
    // setHeal(0.5, 0.1) -> 0:0.5, 1:0.1
    // setHurt(1, 0.5, 1)
}

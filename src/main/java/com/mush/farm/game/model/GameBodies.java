/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.farm.game.model;

import com.mush.farm.game.GameEventQueue;
import com.mush.farm.game.events.BodyEvent;
import java.awt.Point;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author mush
 */
public class GameBodies {

    private final List<Body> bodies;
    private final Map<Integer, Body> bodyIndex;
    private int nextBodyId = 0;

    private final Map<Point, List<Body>> segmentMap;

    public GameBodies() {
        bodies = new LinkedList<>();
        bodyIndex = new HashMap<>();
        segmentMap = new HashMap<>();
    }

    public void update(double time) {
        for (Body body : bodies) {
            body.update(time);
        }
    }

    public void updateSegment(Body body) {
        List<Body> oldSegment = getSegment(body.oldSegmentPoint);
        oldSegment.remove(body);
        getSegment(body.segmentPoint).add(body);
        body.segmentUpdated();
    }

    public List<Body> getSegment(Body body) {
        return getSegment(body.segmentPoint);
    }

    public List<Body> getSegment(Point point) {
        List<Body> segment = segmentMap.get(point);
        if (segment == null) {
            segment = new ArrayList<>();
            segmentMap.put(new Point(point), segment);
        }
        return segment;
    }

    public List<Body> getSegment(int x, int y) {
        return getSegment(new Point(x, y));
    }

    /**
     * Creates new list of results
     *
     * @param body
     * @return
     */
    public List<Body> getNearSegments(Body body) {
        return getNearSegments(body.segmentPoint);
    }

    /**
     * Creates new list of results
     *
     * @param centerPoint
     * @return
     */
    public List<Body> getNearSegments(Point centerPoint) {
        Point point = new Point();
        List<Body> list = new ArrayList<>();
        for (int i = -1; i <= 1; i++) {
            for (int j = -1; j <= 1; j++) {
                point.setLocation(centerPoint.x + i, centerPoint.y + j);
                list.addAll(getSegment(point));
            }
        }
        return list;
    }

    public synchronized Body spawnBody(BodyType type, double x, double y) {
        int id = nextBodyId++;
        Body body = new Body(id, type);
        body.setPosition(x, y);
        bodies.add(body);
        bodyIndex.put(id, body);
        updateSegment(body);
        return body;
    }

    public synchronized boolean unspawn(int id) {
        Body body = bodyIndex.remove(id);
        try {
            if (body != null) {
                bodies.remove(body);
                return true;
            }
            return false;
        } finally {
            GameEventQueue.send(new BodyEvent.Remove(body));
        }
    }

    public void removeFromFreeBodies(Body body) {
        bodies.remove(body);
        getSegment(body).remove(body);
        body.isFreeStanding = false;
    }

    public void addToFreeBodies(Body body, double x, double y) {
        body.isFreeStanding = true;
        body.setPosition(x, y);
        bodies.add(body);
        updateSegment(body);
    }

    // Don't modify from outside
    // Use only for listing through all bodies
    public List<Body> getBodies() {
        return bodies;
    }

    public Body getBody(int id) {
        return bodyIndex.get(id);
    }

    public Body getClosestCreatureBodyTo(Body body, double distance) {
        return getClosestBodyTo(body, true, distance);
    }

    public Body getClosestBodyTo(Body body, double distance) {
        return getClosestBodyTo(body, false, distance);
    }

    public double getDistanceBetween(Body body1, Body body2) {
        Point2D.Double position2 = body2.getPosition();
        Point2D.Double position1 = body1.getPosition();
        double dx = position2.x - position1.x;
        double dy = position2.y - position1.y;
        return Math.sqrt(dx * dx + dy * dy);
    }

    public Body getClosestBodyTo(Body body0, boolean isCreature, double distance) {
        double shortest = Double.POSITIVE_INFINITY;
        Body nearest = null;
        List<Body> nearBodies = getNearSegments(body0);
        for (Body aBody : nearBodies) {
            if (aBody != body0 && (!isCreature || aBody.creature != null)) {
                double dist = getDistanceBetween(body0, aBody);
                if (dist < shortest) {
                    shortest = dist;
                    nearest = aBody;
                }
            }
        }
        nearBodies.clear();
        if (nearest != null && shortest < distance) {
            return nearest;
        }
        return null;
    }

}

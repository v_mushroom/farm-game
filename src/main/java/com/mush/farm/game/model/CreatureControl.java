/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.farm.game.model;

import com.mush.farm.game.GameEventQueue;
import com.mush.farm.game.events.CreatureEvent;
import java.awt.geom.Point2D;

/**
 *
 * @author mush
 */
public class CreatureControl {
    
    private final Creature creature;
    
    public CreatureControl(Creature creature) {
        this.creature = creature;
    }
    
    /**
     * Assigns and normalises the movement direction
     * @param dx
     * @param dy 
     */
    public void setMovingDirection(double dx, double dy) {
        Point2D.Double direction = creature.movementDirection;
        direction.x = dx;
        direction.y = dy;
        if (dx != 0 || dy != 0) {
            // Normalize velocity
            double a = Math.sqrt(dx * dx + dy * dy);
            direction.x /= a;
            direction.y /= a;
        }
    }
    
    public void sendInteract() {
        GameEventQueue.send(new CreatureEvent.Interact(creature));
    }

    public void sendSelect() {
        GameEventQueue.send(new CreatureEvent.Select(creature, true));
    }
    
    public void sendUnselect() {
        GameEventQueue.send(new CreatureEvent.Select(creature, false));
    }
    
    public void sendGive() {
        GameEventQueue.send(new CreatureEvent.Give(creature));
    }

    public void sendPickUp() {
        GameEventQueue.send(new CreatureEvent.PickUp(creature));
    }

    public void sendDrop() {
        GameEventQueue.send(new CreatureEvent.Drop(creature));
    }

    public void sendEquip(int index) {
        GameEventQueue.send(new CreatureEvent.Equip(creature, index));
    }
    
    public void sendEquipLast() {
        int lastIndex = creature.getInventory().size() - 1;
        if (lastIndex >= 0) {
            GameEventQueue.send(new CreatureEvent.Equip(creature, lastIndex));
        }
    }
    
    public void sendUnequip() {
        GameEventQueue.send(new CreatureEvent.Unequip(creature));
    }
    
    public void sendCycleInventory() {
        GameEventQueue.send(new CreatureEvent.CycleInventory(creature));
    }
    
}

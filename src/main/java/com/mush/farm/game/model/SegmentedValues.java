/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.farm.game.model;

/**
 *
 * @author mush
 */
public class SegmentedValues {

    public double[] values;
    private int topIndex;
    private double segmentWidth;

    public SegmentedValues(double... params) {
        values = new double[params.length];

        if (params.length > 0) {
            for (int i = 0; i < params.length; i++) {
                values[i] = params[i];
            }
        }
        topIndex = params.length - 1;
        // len = 5; segment = 1/4
        segmentWidth = topIndex > 0 ? 1.0 / topIndex : 0;
    }
    
    public double evaluate(double point) {
        if (point <= 0) {
            return values[0];
        }
        if (point >= 1) {
            return values[topIndex];
        }
        if (topIndex == 0) {
            return values[0];
        }
        // 0.0..0.5..1.0
        // 0..len-1
        int indexLow = (int) (point * (topIndex));
        int indexHi = indexLow + 1;
        double pointLow = indexLow * segmentWidth;
        double factor = (point - pointLow) / segmentWidth;
        return values[indexLow] * (1 - factor) + values[indexHi] * factor;
    }

}

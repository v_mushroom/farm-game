/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.farm.game.model;

/**
 * Basically the same as Rectangle2D.Double, but with different field names.
 *
 * @author mush
 */
public class Box {

    public double left;
    public double right;
    public double top;
    public double bottom;

    public Box() {
        set(0, 0, 0, 0);
    }

    public Box(double left, double top, double right, double bottom) {
        set(left, top, right, bottom);
    }

    public Box(Box box) {
        set(box);
    }

    public void set(double left, double top, double right, double bottom) {
        this.left = left;
        this.top = top;
        this.right = right;
        this.bottom = bottom;
    }

    public void set(Box box) {
        set(box.left, box.top, box.right, box.bottom);
    }

    public void recalculateActualBox(Box bounds, double x, double y) {
        if (bounds != null) {
            set(
                    x - bounds.left,
                    y - bounds.top,
                    x + bounds.right,
                    y + bounds.bottom
            );
        }
    }

    @Override
    public String toString() {
        return "Box (" + left + ", " + top + ", " + right + ", " + bottom + ")";
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.farm.game.model;

/**
 *
 * @author mush
 */
public enum BodyState {
    AGE, // though not really kept with other states
    FILLED,
    OFFSPRING_PRODUCED;

    public static void setInitialStates(Body body) {
        switch (body.type) {
            case BUCKET:
                body.setState(BodyState.FILLED, false);
                break;
        }
    }

    public static void setInitialStates(Creature creature) {
        creature.body.setState(BodyState.OFFSPRING_PRODUCED, 0);
    }

}

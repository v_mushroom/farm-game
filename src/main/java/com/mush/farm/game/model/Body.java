/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.farm.game.model;

import com.mush.farm.game.GameEventQueue;
import com.mush.farm.game.events.BodyEvent;
import com.mush.farm.game.events.CreatureEvent;
import com.mush.farm.game.logic.GameSizes;
import java.awt.Point;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 *
 * @author mush
 */
public class Body {
    
    private static final double TIME_BETWEEN_AGE_UPDATE_EVENTS = 1.0;

    public final int bodyId;
    /**
     * Don't modify outside
     */
    public final Point2D.Double position = new Point2D.Double(0, 0);
    public final Point segmentPoint = new Point(0, 0);
    public final Point oldSegmentPoint = new Point(0, 0);
    public BodyType type;
    public Creature creature;
    public List<Body> containedBodies;
    public boolean isFreeStanding;
    public Box boundingBox = null;
    public Box actualBox = null;

    public double age; // 0 .. inf
    private Double maxAge;
    public double integrity; // 0 .. 1
    public SegmentedValues healFactor;
    public SegmentedValues hurtFactor;
    private boolean segmentChanged = true;
    private double timeSinceLastAgeUpdateEvent;

    private Map<BodyState, Object> stateMap;

    public Body(int id, BodyType type) {
        this.bodyId = id;
        this.containedBodies = new ArrayList<>();
        this.stateMap = new HashMap<>();
        isFreeStanding = true;
        setType(type);
        BodyState.setInitialStates(this);
    }

    public void setType(BodyType type) {
        this.type = type;
        BodyType.setBoundingBox(this);

        age = 0;
        integrity = 1;
        maxAge = BodyType.getMaxAge(type);
        if (!maxAge.isInfinite()) {
            maxAge *= 0.9 + Math.random() * 0.2;
        }
        timeSinceLastAgeUpdateEvent = 0;

        healFactor = makeFactor(BodyType.getHealParams(type));
        hurtFactor = makeFactor(BodyType.getHurtParams(type));
        
        GameEventQueue.send(new BodyEvent.ChangeType(this, type));
        setState(BodyState.AGE, getAgePercent());
    }

    private SegmentedValues makeFactor(double[] params) {
        if (params == null) {
            return null;
        }
        double[] values = new double[params.length];
        for (int i = 0; i < params.length; i++) {
            values[i] = params[i] * 0.8 + Math.random() * 0.2;
        }
        return new SegmentedValues(values);
    }

    public void update(double time) {
        for (Body body : containedBodies) {
            body.update(time);
        }
        if (creature != null && creature.hasEquipped()) {
            creature.getEquipped().update(time);
        }

        age += time;
        if (age > maxAge || integrity <= 0) {
            decay();
        } else {
            timeSinceLastAgeUpdateEvent += time;
            if (timeSinceLastAgeUpdateEvent > TIME_BETWEEN_AGE_UPDATE_EVENTS) {
                timeSinceLastAgeUpdateEvent = 0;
//                GameEventQueue.send(new BodyEvent.ChangeState(this, BodyState.AGE));
                setState(BodyState.AGE, getAgePercent());
            }
            
            heal(time);
        }
    }

    public void hurt(double rawAmount) {
        if (hurtFactor != null) {
            double factor = hurtFactor.evaluate(getAgePercent());
            integrity -= rawAmount * factor;
        } else {
            integrity -= rawAmount;
        }
    }

    private void heal(double seconds) {
        if (integrity >= 1) {
            return;
        }
        if (healFactor != null) {
            double factor = healFactor.evaluate(getAgePercent());
            integrity += factor * seconds;
        }
    }

    public void setPosition(double x, double y) {
        position.setLocation(x, y);
        updateAfterPositionChange();
    }

    public void setPosition(Point2D.Double position) {
        this.position.setLocation(position);
        updateAfterPositionChange();
    }

    public void offsetPosition(double dx, double dy) {
        this.position.x += dx;
        this.position.y += dy;
        updateAfterPositionChange();
    }

    private void updateAfterPositionChange() {
        if (isFreeStanding) {
            recalculateActualBox();
            recalculateSegment();
        }
        updateContainedBodiesPosition();
    }

    public boolean hasSegmentChanged() {
        return segmentChanged;
    }

    public void segmentUpdated() {
        segmentChanged = false;
        oldSegmentPoint.setLocation(segmentPoint);
    }

    /**
     * Don't modify the position outside
     *
     * @return
     */
    public Point2D.Double getPosition() {
        return position;
    }

    public void setBoundingBox(int left, int top, int right, int bottom) {
        if (boundingBox == null) {
            boundingBox = new Box(left, top, right, bottom);
            actualBox = new Box();
        } else {
            boundingBox.set(left, top, right, bottom);
        }
        recalculateActualBox();
    }

    public void setBoundingBox(Box box) {
        if (box == null) {
            boundingBox = null;
            actualBox = null;
            return;
        }
        if (boundingBox == null) {
            boundingBox = new Box(box);
            actualBox = new Box();
        } else {
            boundingBox.set(box);
        }
        recalculateActualBox();
    }

    private void recalculateActualBox() {
        if (actualBox != null) {
            actualBox.recalculateActualBox(boundingBox, position.x, position.y);
        }
    }

    private void recalculateSegment() {
        int segmentX = (int) (position.x / GameSizes.TILE_SIZE);
        int segmentY = (int) (position.y / GameSizes.TILE_SIZE);

        segmentChanged = segmentX != segmentPoint.x || segmentY != segmentPoint.y;
        oldSegmentPoint.setLocation(segmentPoint);
        segmentPoint.setLocation(segmentX, segmentY);
    }

    private void decay() {
        setType(BodyType.getDecaysTo(type));
        if (creature != null) {
            GameEventQueue.send(new CreatureEvent.Die(creature));
        }
    }

    public double getAgePercent() {
        return age / maxAge;
    }

    public void updateContainedBodiesPosition() {
        for (Body body : containedBodies) {
            body.setPosition(position);
        }
        if (creature != null && creature.hasEquipped()) {
            creature.getEquipped().setPosition(position);
        }
    }

    public void setState(BodyState key, Object value) {
        if (changeState(key, value)) {
            GameEventQueue.send(new BodyEvent.ChangeState(this, key));
        }
    }

    private boolean changeState(BodyState key, Object value) {
        Object previousValue = stateMap.put(key, value);
        if (previousValue == null && value != null) {
            return true;
        } else {
            return !Objects.equals(value, previousValue);
        }
    }

    public Object getState(BodyState key) {
        return stateMap.get(key);
    }
    
    public Set<BodyState> getStates() {
        return stateMap.keySet();
    }
    
    public Map<BodyState, Object> getStateMap() {
        return stateMap;
    }

}

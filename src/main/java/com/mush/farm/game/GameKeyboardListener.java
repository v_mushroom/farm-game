/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.farm.game;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author mush
 */
public class GameKeyboardListener implements KeyListener {

    private final GameControl control;
    private final Map<Integer, Boolean> keyStateMap;

    public GameKeyboardListener(GameControl control) {
        this.control = control;
        this.keyStateMap = new HashMap<>();
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
        switch (e.getKeyCode()) {
            case KeyEvent.VK_LEFT:
            case KeyEvent.VK_A:
                control.joystick.pushLeft();
                break;
            case KeyEvent.VK_RIGHT:
            case KeyEvent.VK_D:
                control.joystick.pushRight();
                break;
            case KeyEvent.VK_UP:
            case KeyEvent.VK_W:
                control.joystick.pushUp();
                break;
            case KeyEvent.VK_DOWN:
            case KeyEvent.VK_S:
                control.joystick.pushDown();
                break;
            default:
                onKeyDown(e.getKeyCode());
        }

        control.applyJoystick();
    }

    @Override
    public void keyReleased(KeyEvent e) {
        switch (e.getKeyCode()) {
            case KeyEvent.VK_LEFT:
            case KeyEvent.VK_A:
                control.joystick.releaseLeft();
                break;
            case KeyEvent.VK_RIGHT:
            case KeyEvent.VK_D:
                control.joystick.releaseRight();
                break;
            case KeyEvent.VK_UP:
            case KeyEvent.VK_W:
                control.joystick.releaseUp();
                break;
            case KeyEvent.VK_DOWN:
            case KeyEvent.VK_S:
                control.joystick.releaseDown();
                break;
            default:
                onKeyUp(e.getKeyCode());
        }

        control.applyJoystick();
    }

    private void onKeyDown(int keyCode) {
        Boolean previous = keyStateMap.put(keyCode, Boolean.TRUE);
        if (!Boolean.TRUE.equals(previous)) {
            control.onKeyDown(keyCode);
        }
    }

    private void onKeyUp(int keyCode) {
        Boolean previous = keyStateMap.put(keyCode, Boolean.FALSE);
        if (!Boolean.FALSE.equals(previous)) {
            control.onKeyUp(keyCode);
        }
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.farm.game.events;

import com.mush.farm.game.model.Body;
import com.mush.farm.game.model.BodyState;
import com.mush.farm.game.model.BodyType;

/**
 *
 * @author mush
 */
public abstract class BodyEvent extends GameEvent {

    public final int bodyId;

    public BodyEvent(Body body) {
        this.bodyId = body.bodyId;
    }

    public BodyEvent(int bodyId) {
        this.bodyId = bodyId;
    }

    public static class ChangeType extends BodyEvent {

        public final BodyType type;

        public ChangeType(Body body, BodyType type) {
            super(body);
            this.type = type;
        }

        public ChangeType(int bodyId, BodyType type) {
            super(bodyId);
            this.type = type;
        }
    }

    public static class ChangeState extends BodyEvent {

        public final BodyState stateKey;

        public ChangeState(Body body, BodyState stateKey) {
            super(body);
            this.stateKey = stateKey;
        }
    }

    public static class Remove extends BodyEvent {

        public Remove(Body body) {
            super(body);
        }
    }
}

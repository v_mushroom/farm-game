/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.farm.game;

import com.mush.farm.game.events.ControlEvent;
import com.mush.farm.game.events.GenericGameEvent;
import com.mush.farm.game.model.Creature;
import com.mush.farm.game.model.MapObjectType;
import java.awt.event.KeyEvent;

/**
 *
 * @author mush
 */
public class GameControl {

    public final GameControlJoystick joystick;
    private final Game game;
    private boolean isHoldingShift;

    GameControl(Game game) {
        this.game = game;
        joystick = new GameControlJoystick();
    }

    public void onKeyDown(int keyCode) {
        switch (keyCode) {
            case KeyEvent.VK_ESCAPE:
                System.exit(0);
                break;
            case KeyEvent.VK_V:
                if (isHoldingShift) {
                    toggleShowVectors();
                } else {
                    toggleShowStats();
                }
                break;
            case KeyEvent.VK_C:
                toggleCollisions();
                break;
            case KeyEvent.VK_1:
                debugEvent("setTile", MapObjectType.DIRT);
                break;
            case KeyEvent.VK_2:
                debugEvent("setTile", MapObjectType.WATER);
                break;
            case KeyEvent.VK_3:
                debugEvent("setTile", MapObjectType.POTATO_PLANTED);
                break;
            case KeyEvent.VK_4:
                debugEvent("setTile", MapObjectType.STONE_WALL);
                break;
            case KeyEvent.VK_5:
                debugEvent("setTile", MapObjectType.DIRT_HOLE);
                break;
            case KeyEvent.VK_E:
                if (isHoldingShift) {
                    actionPlayerSelect(false);
                } else {
                    actionPlayerSelect(true);
                }
                break;
            case KeyEvent.VK_Q:
                if (isHoldingShift) {
                    actionPlayerPickUp(false);
                } else {
                    actionPlayerPickUp(true);
                }
                break;
            case KeyEvent.VK_R:
                actionPlayerEquip();
                break;
            case KeyEvent.VK_F:
                actionPlayerInteract();
                break;
            case KeyEvent.VK_G:
                actionPlayerGive();
                break;
            case KeyEvent.VK_I:
                actionCycleInventory();
                break;
            case KeyEvent.VK_P:
                togglePause();
                break;
            case KeyEvent.VK_T:
                changeCreature();
                break;
            case KeyEvent.VK_SHIFT:
                isHoldingShift = true;
                break;
        }
    }

    public void onKeyUp(int keyCode) {
        switch (keyCode) {
            case KeyEvent.VK_SHIFT:
                isHoldingShift = false;
                break;
        }
    }

    public void debugEvent(String name, Object payload) {
        GameEventQueue.send(new GenericGameEvent(name, payload));
    }

    void toggleShowStats() {
        GameEventQueue.send(new ControlEvent(ControlEvent.Action.TOGGLE_STATS));
    }

    void toggleShowVectors() {
        GameEventQueue.send(new ControlEvent(ControlEvent.Action.TOGGLE_VECTORS));
    }

    void toggleCollisions() {
        GameEventQueue.send(new ControlEvent(ControlEvent.Action.TOGGLE_COLLISIONS));
    }

    void togglePause() {
        GameEventQueue.send(new ControlEvent(ControlEvent.Action.PAUSE));
    }

    void changeCreature() {
        GameEventQueue.send(new ControlEvent(ControlEvent.Action.CHANGE_CREATURE));
    }

    public void applyJoystick() {
        if (joystick.isModified()) {
            Creature player = game.getPlayer();
            if (player != null) {
                player.control.setMovingDirection(joystick.getXJoystick(), joystick.getYJoystick());
            }
        }
    }

    public void actionPlayerInteract() {
        Creature player = game.getPlayer();
        if (player != null) {
            player.control.sendInteract();
        }
    }

    public void actionPlayerGive() {
        Creature player = game.getPlayer();
        if (player != null) {
            player.control.sendGive();
        }
    }

    public void actionPlayerSelect(boolean pickUp) {
        Creature player = game.getPlayer();
        if (player != null) {
            if (pickUp) {
                player.control.sendSelect();
            } else {
                player.control.sendUnselect();
            }
        }
    }

    public void actionPlayerPickUp(boolean pickUp) {
        Creature player = game.getPlayer();
        if (player != null) {
            if (pickUp) {
                player.control.sendPickUp();
            } else {
                player.control.sendDrop();
            }
        }
    }

//    public void actionPlayerDrop() {
//        Creature player = game.getPlayer();
//        if (player != null) {
//            player.control.sendDrop();
//        }
//    }
    public void actionPlayerEquip() {
        Creature player = game.getPlayer();
        if (player != null) {
            if (player.getEquipped() == null) {
                player.control.sendEquipLast();
            } else {
                player.control.sendUnequip();
            }
        }
    }

    public void actionCycleInventory() {
        Creature player = game.getPlayer();
        if (player != null) {
            player.control.sendCycleInventory();
        }
    }

}

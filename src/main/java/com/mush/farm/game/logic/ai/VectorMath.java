/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.farm.game.logic.ai;

import java.awt.geom.Point2D;

/**
 *
 * @author mush
 */
public class VectorMath {

    static private Point2D.Double value(double x, double y, Point2D.Double result) {
        if (result == null) {
            result = new Point2D.Double(x, y);
        } else {
            result.setLocation(x, y);
        }
        
        return result;
    }
    
    static public Point2D.Double rotate(Point2D.Double vector, double fi, Point2D.Double result) {
        double cos = Math.cos(fi);
        double sin = Math.sin(fi);
        
        double x = cos * vector.x - sin * vector.y;
        double y = sin * vector.x + cos * vector.y;
        
        return value(x, y, result);
    }
    
    static public Point2D.Double multiply(Point2D.Double vector, double s, Point2D.Double result) {
        double x = vector.x * s;
        double y = vector.y * s;
        
        return value(x, y, result);
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.farm.game.logic.ai;

import com.mush.farm.game.Game;
import com.mush.farm.game.logic.GameSizes;
import com.mush.farm.game.model.Box;
import com.mush.farm.game.model.Creature;
import java.awt.geom.Point2D;

/**
 *
 * @author mush
 */
public class WayPointNavigator {

    private final Game game;
    private final Creature creature;
    private Point2D.Double wayPoint = new Point2D.Double();
    private Point2D.Double direction = new Point2D.Double();

    private boolean moving;
    private boolean blocked;
    private boolean finished;

    public WayPointNavigator(Game game, Creature creature) {
        this.game = game;
        this.creature = creature;
        wayPoint.setLocation(this.creature.body.getPosition());
    }

    public void update() {
        Point2D.Double position = creature.body.getPosition();
        double distance = wayPoint.distance(position);

        if (distance > GameSizes.TILE_SIZE * 0.25) {
            goTowardsWaypoint();
            checkForCollision();
            creature.control.setMovingDirection(direction.x, direction.y);
        } else {
            creature.control.setMovingDirection(0, 0);
            finished = true;
            moving = false;
        }
    }

    private void goTowardsWaypoint() {
        Point2D.Double position = creature.body.getPosition();
        double dx = wayPoint.x - position.x;
        double dy = wayPoint.y - position.y;
        double d = Math.sqrt(dx * dx + dy * dy);
        if (d != 0) {
            dx /= d;
            dy /= d;
        }
        direction.setLocation(dx, dy);
    }

    private boolean checkForCollisionInDirection(Point2D.Double leftDirection, Box movingBox) {
        Point2D.Double position = creature.body.getPosition();
        Point2D.Double leftOffset = VectorMath.multiply(leftDirection, GameSizes.TILE_SIZE * 0.25, null);
        movingBox.recalculateActualBox(creature.body.boundingBox, position.x + leftOffset.x, position.y + leftOffset.y);
        
        if (game.movementLogic.checkMapCollision(movingBox)) {
            return true;
        }
        
        if (game.movementLogic.checkBodyCollision(creature.body, movingBox)) {
            return true;
        }
        
        return false;
    }

    private void checkForCollision() {
        blocked = false;
        moving = true;

        Box box = new Box();

        boolean collisionInFront = checkForCollisionInDirection(direction, box);

        if (!collisionInFront) {
            return;
        }

        Point2D.Double leftDirection = VectorMath.rotate(direction, Math.PI * 0.1, null);
        Point2D.Double rightDirection = VectorMath.rotate(direction, -Math.PI * 0.1, null);
        boolean collisionLeft = checkForCollisionInDirection(leftDirection, box);
        boolean collisionRight = checkForCollisionInDirection(rightDirection, box);

        if (!collisionLeft) {
            direction.setLocation(leftDirection);
            return;
        }

        if (!collisionRight) {
            direction.setLocation(rightDirection);
            return;
        }

        leftDirection = VectorMath.rotate(direction, Math.PI * 0.25, leftDirection);
        rightDirection = VectorMath.rotate(direction, -Math.PI * 0.25, rightDirection);
        collisionLeft = checkForCollisionInDirection(leftDirection, box);
        collisionRight = checkForCollisionInDirection(rightDirection, box);

        if (!collisionLeft) {
            direction.setLocation(leftDirection);
            return;
        }

        if (!collisionRight) {
            direction.setLocation(rightDirection);
            return;
        }

        direction.setLocation(0, 0);
        blocked = true;
        moving = false;
    }

    public void setWayPoint(double x, double y) {
        wayPoint.setLocation(x, y);
        moving = true;
        finished = false;
        blocked = false;
    }

    public Point2D.Double getWayPoint() {
        return wayPoint;
    }

    public boolean isMoving() {
        return moving;
    }

    public boolean isBlocked() {
        return blocked;
    }

    public boolean isFinished() {
        return finished;
    }

}

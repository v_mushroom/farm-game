/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.farm.game.logic;

import com.mush.farm.game.Game;
import com.mush.farm.game.GameEventQueue;
import com.mush.farm.game.events.InteractionEvent;
import com.mush.farm.game.events.MapEvent;
import com.mush.farm.game.model.Body;
import com.mush.farm.game.model.BodyState;
import com.mush.farm.game.model.MapObjectType;

/**
 *
 * @author mush
 */
public class GameInteractionsLogic {

    private Game game;

    public GameInteractionsLogic(Game game) {
        this.game = game;
    }

    public void onEvent(InteractionEvent.BodyOnBody event) {
        System.out.println("interaction by c.id:" + event.creatureId
                + " with tool.type:" + event.toolType
                + " on target.type:" + event.targetType);
        switch (event.toolType) {
            case HOG:
                hogOnBody(event);
                break;
        }
    }

    public void onEvent(InteractionEvent.BodyOnMapObject event) {
        System.out.println("interaction by c.id:" + event.creatureId
                + " with tool.type:" + event.toolType
                + " on target.type:" + event.targetType);

        switch (event.toolType) {
            case BUCKET:
                bucketOnTile(event);
                break;
            case SHOVEL:
                shovelOnTile(event);
                break;
            case HOG:
                hogOnTile(event);
        }
    }
    
    private void bucketOnTile(InteractionEvent.BodyOnMapObject event) {
        Body bucket = game.bodies.getBody(event.toolId);
        if (bucket != null) {
            Boolean filled = (Boolean) bucket.getState(BodyState.FILLED);
            if (Boolean.TRUE.equals(filled)) {
                bucketFullOnTile(bucket, event);
            } else {
                bucketEmptyOnTile(bucket, event);
            }
        }
    }

    private void bucketFullOnTile(Body bucket, InteractionEvent.BodyOnMapObject event) {
        if (event.targetType == MapObjectType.DIRT_HOLE) {
            game.gameMap.setTile(event.targetU, event.targetV, MapObjectType.WATER);
            bucket.setState(BodyState.FILLED, false);
        }
    }

    private void bucketEmptyOnTile(Body bucket, InteractionEvent.BodyOnMapObject event) {
        if (event.targetType == MapObjectType.WATER) {
            bucket.setState(BodyState.FILLED, true);
        }
    }

    private void shovelOnTile(InteractionEvent.BodyOnMapObject event) {
        switch (event.targetType) {
            case DIRT:
            case GRASS:
            case ORGANIC_RUBBLE:
                game.gameMap.setTile(event.targetU, event.targetV, MapObjectType.DIRT_HOLE);
                break;
        }
    }

    private void hogOnTile(InteractionEvent.BodyOnMapObject event) {
        switch (event.targetType) {
            case DIRT:
            case ORGANIC_RUBBLE:
            case GRASS:
                game.gameMap.setTile(event.targetU, event.targetV, MapObjectType.DIRT_STOMPED);
                break;
            case POTATO_PLANT:
            case POTATO_PLANTED:
            case POTATO_SAPLING:
            case STONE_WALL:
                GameEventQueue.send(new MapEvent.Damage(event.targetU, event.targetV, 0.2));
                break;
        }
    }

    private void hogOnBody(InteractionEvent.BodyOnBody event) {
        Body target = game.bodies.getBody(event.targetId);
        target.hurt(0.2);
    }

}

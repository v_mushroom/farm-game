/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.farm.game.logic.ai;

import com.mush.farm.game.Game;
import com.mush.farm.game.logic.GameSizes;
import com.mush.farm.game.model.Creature;
import com.mush.farm.game.model.MapObjectType;
import java.awt.Point;
import java.awt.geom.Point2D;

/**
 *
 * @author mush
 */
public class RoutePlanner {
    
    private final Game game;
    private final Creature creature;
    private final WayPointNavigator navigator;
    
    public RoutePlanner(Game game, Creature creature, WayPointNavigator navigator) {
        this.game = game;
        this.creature = creature;
        this.navigator = navigator;
    }
    
    public void recalculate() {
        Point2D.Double position = creature.body.getPosition();
        Point2D.Double vector = new Point2D.Double();

        int tries = 10;
        for (int i = 0; i < tries; i++) {

            calculateRandomVector(vector);
            boolean collidable = false;

            for (int j = 0; j < 5; j++) {
                Point mapPoint = GameSizes.getTileCoordinates(
                        position.x + vector.x * (GameSizes.TILE_SIZE * (0.5 + Math.random())),
                        position.y + vector.y * (GameSizes.TILE_SIZE * (0.5 + Math.random())));

                MapObjectType type = game.gameMap.getMapObjectType(mapPoint.x, mapPoint.y);

                if (type == null || !MapObjectType.isCollidable(type)) {
                    collidable = true;
                    break;
                }
            }

            if (collidable) {
                break;
            }
        }

        double distance = GameSizes.TILE_SIZE * (1 + Math.random() * 5);

//        wayPoint.setLocation(position.x + vector.x * distance, position.y + vector.y * distance);
        navigator.setWayPoint(position.x + vector.x * distance, position.y + vector.y * distance);
    }

    private void calculateRandomVector(Point2D.Double vector) {
        double dx = -1 + Math.random() * 2;
        double dy = -1 + Math.random() * 2;
        double d = Math.sqrt(dx * dx + dy * dy);
        if (d != 0) {
            dx /= d;
            dy /= d;
        }
        vector.x = dx;
        vector.y = dy;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.farm.game.logic.path;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author mush
 */
public class PathFinder {

    private class SearchNode {

        int nodeId;
        int fromNodeId;
        double travelDistance;
        double endDistance;
        double totalDistance;
    }

    private class SearchNodeComparator implements Comparator<SearchNode> {

        @Override
        public int compare(SearchNode o1, SearchNode o2) {
            return Double.compare(o1.totalDistance, o2.totalDistance);
        }
    }

    private Graph graph;
    private int startId;
    private int endId;
    private List<SearchNode> queue;
    private boolean queueDirty;
    private Set<Integer> queuedIdSet;
    private Map<Integer, SearchNode> searchMap;
    private final SearchNodeComparator searchNodeComparator = new SearchNodeComparator();

    public PathFinder(Graph graph) {
        this.graph = graph;
        this.queue = new ArrayList<>();
        this.queuedIdSet = new HashSet<>();
        this.searchMap = new HashMap<>();
    }

    public void setStartEnd(int startId, int endId) {
        this.startId = startId;
        this.endId = endId;
    }

    public List<GraphNode> find() {
        start();

        SearchNode endNode = search();
        if (endNode == null) {
            return null;
        }

        List<GraphNode> path = makePath(endNode);
        clear();
        return path;
    }

    private void addToQueue(SearchNode node) {
        node.totalDistance = node.travelDistance + node.endDistance;
        queue.add(node);
        queuedIdSet.add(node.nodeId);
        searchMap.put(node.nodeId, node);
        queueDirty = true;
    }

    private void replaceInQueue(SearchNode node) {
        node.totalDistance = node.travelDistance + node.endDistance;
        SearchNode queuedNode = findInQueue(node.nodeId);
        queue.remove(queuedNode);
        queue.add(node);
        searchMap.put(node.nodeId, node);
        queueDirty = true;
    }

    private SearchNode findInQueue(int id) {
        for (SearchNode node : queue) {
            if (node.nodeId == id) {
                return node;
            }
        }
        return null;
    }

    private SearchNode popFromQueue() {
        SearchNode first = topOfQueue();
        queue.remove(first);
        queuedIdSet.remove(first.nodeId);
        return first;
    }

    private SearchNode topOfQueue() {
        if (queueDirty) {
            queueDirty = false;
            Collections.sort(queue, searchNodeComparator);
        }
        if (queue.isEmpty()) {
            return null;
        } else {
            return queue.get(0);
        }
    }

    private boolean isInQueue(int id) {
        return queuedIdSet.contains(id);
    }

    private boolean isUnprocessed(int id) {
        return !searchMap.containsKey(id);
    }

    private List<GraphNode> makePath(SearchNode endNode) {
        List<GraphNode> path = new ArrayList<>();
        SearchNode searchNode = endNode;
        boolean finished = false;

        while (!finished) {
            GraphNode node = graph.nodes.get(searchNode.nodeId);
            path.add(node);

            if (searchNode.nodeId == startId) {
                finished = true;
            } else {
                if (searchNode.nodeId != searchNode.fromNodeId) {
                    searchNode = searchMap.get(searchNode.fromNodeId);
                } else {
                    return null;
                }
            }
        }

        Collections.reverse(path);
        return path;
    }

    private SearchNode search() {
        while (!queue.isEmpty()) {
            SearchNode best = popFromQueue();

            expand(best.nodeId);

            SearchNode top = topOfQueue();
            if (top == null) {
                return null;
            }
            if (top.nodeId == endId) {
                return top;
            }
        }
        return null;
    }

    private void clear() {
        queue.clear();
        queuedIdSet.clear();
        searchMap.clear();
        queueDirty = false;
    }

    private void start() {
        clear();

        GraphNode node = graph.nodes.get(startId);
        GraphNode endNode = graph.nodes.get(endId);

        SearchNode searchNode = new SearchNode();
        searchNode.nodeId = startId;
        searchNode.fromNodeId = startId;
        searchNode.travelDistance = 0;
        searchNode.endDistance = endNode.position.distance(node.position);

        addToQueue(searchNode);
    }

    private void expand(int nodeId) {
        GraphNode node = graph.nodes.get(nodeId);
        SearchNode searchNode = searchMap.get(nodeId);
        GraphNode endNode = graph.nodes.get(endId);
        for (Map.Entry<Integer, Double> e : node.connections.entrySet()) {
            int peerId = e.getKey();
            double stepDistance = e.getValue();
            GraphNode peerNode = graph.nodes.get(peerId);
            expandFrom(nodeId, searchNode.travelDistance + stepDistance, peerNode, endNode);
        }
    }

    private void expandFrom(int fromId, double travelDistance, GraphNode node, GraphNode endNode) {
        if (isInQueue(node.id)) {
            SearchNode searchNode = searchMap.get(node.id);
            if (travelDistance < searchNode.travelDistance) {
                searchNode.fromNodeId = fromId;
                searchNode.travelDistance = travelDistance;
                searchNode.totalDistance = searchNode.travelDistance + searchNode.endDistance;
                replaceInQueue(searchNode);
            }
        } else if (isUnprocessed(node.id)) {
            SearchNode searchNode = new SearchNode();
            searchNode.nodeId = node.id;
            searchNode.fromNodeId = fromId;
            searchNode.travelDistance = travelDistance;
            searchNode.endDistance = endNode.position.distance(node.position);
            addToQueue(searchNode);
        }
    }

}

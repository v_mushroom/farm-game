/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.farm.game.logic.path;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author mush
 */
public class Graph {
    
    public Map<Integer, GraphNode> nodes;
    private int nextId = 0;
    
    public Graph() {
        nodes = new HashMap<>();
    }
    
    public int addNode(double x, double y) {
        int id = nextId;
        nextId++;
        GraphNode node = new GraphNode(id, x, y);
        nodes.put(id, node);
        return id;
    }
    
    public void connect(int id1, int id2) {
        if (id1 == id2) {
            return;
        }
        
        GraphNode node1 = nodes.get(id1);
        GraphNode node2 = nodes.get(id2);
        
        if (node1 == null || node2 == null) {
            return;
        }
        
        double distance = node1.position.distance(node2.position);
        
        if (!node1.connections.containsKey(id2)) {
            node1.connections.put(id2, distance);
        }
        if (!node2.connections.containsKey(id1)) {
            node2.connections.put(id1, distance);
        }
    }
    
    public void disconnect(int id1, int id2) {
        GraphNode node1 = nodes.get(id1);
        GraphNode node2 = nodes.get(id2);
        
        if (node1 == null || node2 == null) {
            return;
        }

        node1.connections.remove(id2);
        node2.connections.remove(id1);
    }
}

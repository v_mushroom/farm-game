/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.farm.game.logic.path;

import java.awt.geom.Point2D;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author mush
 */
public class GraphNode {

    public int id;
    public Point2D.Double position;
    public Map<Integer, Double> connections;

    public GraphNode(int id, double x, double y) {
        this.id = id;
        this.position = new Point2D.Double(x, y);
        connections = new HashMap<>();
    }
}

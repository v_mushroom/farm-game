/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.farm.game.logic.ai;

import com.mush.farm.game.Game;
import com.mush.farm.game.model.Creature;

/**
 *
 * @author mush
 */
public class CreatureAi {

    private double timeSinceRecalculation;
    private double nextRecalculationTime = 3.0;

    private Creature creature;
    private final Game game;
    
    public RoutePlanner planner;
    public WayPointNavigator navigator;

    public CreatureAi(Game game) {
        this.game = game;
    }

    public void assignToCreature(Creature creature) {
        this.creature = creature;
        this.creature.creatureAi = this;
        this.creature.aiEnabled = true;
        timeSinceRecalculation = nextRecalculationTime;

        navigator = new WayPointNavigator(game, creature);
        planner = new RoutePlanner(game, creature, navigator);
    }
    
    public void removeFromCreature() {
        this.creature.creatureAi = null;
        this.creature.aiEnabled = false;
        navigator = null;
        planner = null;
    }

    public void update(double seconds) {
        if (creature == null) {
            return;
        }

        timeSinceRecalculation += seconds;
        if (timeSinceRecalculation > nextRecalculationTime) {
            planner.recalculate();
            timeSinceRecalculation = 0;
        }

        navigator.update();
                
        if (!navigator.isMoving()) {
            timeSinceRecalculation = nextRecalculationTime;
        }
    }

}

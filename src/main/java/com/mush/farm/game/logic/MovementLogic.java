/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.farm.game.logic;

import com.mush.farm.game.Game;
import com.mush.farm.game.GameEventQueue;
import com.mush.farm.game.events.CreatureEvent;
import com.mush.farm.game.model.Body;
import com.mush.farm.game.model.Box;
import com.mush.farm.game.model.Creature;
import com.mush.farm.game.model.MapObjectType;
import java.awt.Point;
import java.awt.geom.Point2D;
import java.util.List;

/**
 *
 * @author mush
 */
public class MovementLogic {

    private final static Box TILE_BOUNDS = new Box(
            GameSizes.TILE_SIZE / 2,
            GameSizes.TILE_SIZE / 2,
            GameSizes.TILE_SIZE / 2,
            GameSizes.TILE_SIZE / 2);

    private final Game game;

    public MovementLogic(Game game) {
        this.game = game;
    }

    public void onEvent(CreatureEvent.Move event) {
        Creature creature = game.creatures.getCreature(event.creatureId);
        if (creature != null) {
            move(creature, event.xOffset, event.yOffset);
        }
    }

    private void move(Creature creature, double xOffset, double yOffset) {
        Point2D.Double offset = new Point2D.Double(xOffset, yOffset);

        if (creature.body.boundingBox != null) {
            checkMove(creature, offset);
        }

        creature.body.offsetPosition(offset.x, offset.y);

        if (creature.body.hasSegmentChanged()) {
            game.bodies.updateSegment(creature.body);
        }
    }

    private void calculateNextActualBox(Box next, Body body, Point2D.Double offset) {
        Point2D.Double currentPos = body.getPosition();
        next.recalculateActualBox(body.boundingBox, currentPos.x + offset.x, currentPos.y + offset.y);
    }

    private void checkMove(Creature creature, Point2D.Double offset) {
        Box nextActualBox = new Box();
        calculateNextActualBox(nextActualBox, creature.body, offset);

        creature.collisionTilePoints.clear();

        List<Body> nearBodies = game.bodies.getNearSegments(creature.body);
        for (Body body : nearBodies) {
            if (body != creature.body) {
                if (checkMoveCollision(nextActualBox, body.actualBox, offset)) {
                    GameEventQueue.send(new CreatureEvent.BodyCollision(creature, body));
                    calculateNextActualBox(nextActualBox, creature.body, offset);
                }
            }
        }
        nearBodies.clear();

        if (!game.getMapCollisionsEnabled()) {
            return;
        }

        // Reusable box
        Box box = new Box();

        Point centerTilePoint = GameSizes.getTileCoordinates(creature.body.getPosition());

        // Check first centre
        checkMoveToTile(0, 0, centerTilePoint, creature, box, nextActualBox, offset);

        // Then check direct neighbours
        checkMoveToTile(-1, 0, centerTilePoint, creature, box, nextActualBox, offset);
        checkMoveToTile(0, -1, centerTilePoint, creature, box, nextActualBox, offset);
        checkMoveToTile(+1, 0, centerTilePoint, creature, box, nextActualBox, offset);
        checkMoveToTile(0, +1, centerTilePoint, creature, box, nextActualBox, offset);

        // Then check diagonal neighbours
        checkMoveToTile(-1, -1, centerTilePoint, creature, box, nextActualBox, offset);
        checkMoveToTile(+1, -1, centerTilePoint, creature, box, nextActualBox, offset);
        checkMoveToTile(+1, +1, centerTilePoint, creature, box, nextActualBox, offset);
        checkMoveToTile(-1, +1, centerTilePoint, creature, box, nextActualBox, offset);
    }

    private Box boxForTile(int u, int v, Box box0) {
        Box box = box0 != null ? box0 : new Box();
        box.recalculateActualBox(TILE_BOUNDS, GameSizes.getTileCenterX(u), GameSizes.getTileCenterY(v));
        return box;
    }

    private void checkMoveToTile(int dx, int dy, Point centerTilePoint, Creature creature, Box reusableBox, Box nextActualBox, Point2D.Double offset) {
        Point tilePoint = new Point(centerTilePoint.x + dx, centerTilePoint.y + dy);
        MapObjectType tileType = checkTileCollision(tilePoint.x, tilePoint.y, nextActualBox, reusableBox, offset);
        if (tileType != null) {
            GameEventQueue.send(new CreatureEvent.MapCollision(creature, tileType));
            calculateNextActualBox(nextActualBox, creature.body, offset);
            creature.collisionTilePoints.add(tilePoint);
        }
    }

    public boolean checkBodyCollision(Body body, Box movingBox) {
        List<Body> nearBodies = game.bodies.getNearSegments(body);
        try {
            for (Body aBody : nearBodies) {
                if (aBody != body) {
                    if (checkMoveCollision(movingBox, aBody.actualBox, null)) {
                        return true;
                    }
                }
            }
            return false;
        } finally {
            nearBodies.clear();
        }
    }

    public boolean checkMapCollision(Box movingBox) {
        Box reusableTileBox = new Box();

        // First check top left corner
        Point topLeft = GameSizes.getTileCoordinates(movingBox.left, movingBox.top);

        if (checkTileCollision(topLeft.x, topLeft.y, movingBox, reusableTileBox)) {
            return true;
        }

        // No hit, next check bottom right corner
        Point bottomRight = GameSizes.getTileCoordinates(movingBox.right, movingBox.bottom);

        if (bottomRight.equals(topLeft)) {
            // Top left was already checked
            return false;
        }

        if (checkTileCollision(bottomRight.x, bottomRight.y, movingBox, reusableTileBox)) {
            return true;
        }

        // No hit, check remaining two corners
        if (topLeft.x == bottomRight.x || topLeft.y == bottomRight.y) {
            // Only two differnet points, both have already been checked
            return false;
        }

        // First of the corners to hit will return first
        if (checkTileCollision(topLeft.x, bottomRight.y, movingBox, reusableTileBox)
                || checkTileCollision(bottomRight.x, topLeft.y, movingBox, reusableTileBox)) {
            return true;
        }

        return false;
    }

    public boolean checkTileCollision(int u, int v, Box movingBox, Box reusableTileBox) {
        return checkTileCollision(u, v, movingBox, reusableTileBox, null) != null;
    }

    public MapObjectType checkTileCollision(int u, int v, Box movingBox, Box reusableTileBox, Point2D.Double offset) {
        MapObjectType type = game.gameMap.getMapObjectType(u, v);
        if (MapObjectType.isCollidable(type)) {
            Box tileBox = boxForTile(u, v, reusableTileBox);
            if (checkMoveCollision(movingBox, tileBox, offset)) {
                return type;
            }
        }
        return null;
    }

//    public boolean burek(Box movingBox) {
//        List<Body> nearBodies = game.bodies.getNearSegments(creature.body);
//        for (Body body : nearBodies) {
//            if (body != creature.body) {
//                if (checkMoveCollision(nextActualBox, body.actualBox, offset)) {
//                    GameEventQueue.send(new CreatureEvent.BodyCollision(creature, body));
//                    calculateNextActualBox(nextActualBox, creature.body, offset);
//                }
//            }
//        }
//        nearBodies.clear();        
//    }
    public static boolean checkMoveCollision(Box boxA, Box boxB, Point2D.Double offset) {
        if (boxA == null || boxB == null) {
            return false;
        }

        double bRightLead = boxB.right - boxA.left;
        double aRightLead = boxA.right - boxB.left;
        double bBottomLead = boxB.bottom - boxA.top;
        double aBottomLead = boxA.bottom - boxB.top;

        if (bRightLead > 0 && aRightLead > 0 && bBottomLead > 0 && aBottomLead > 0) {
            if (offset != null) {
                double correctionX = aRightLead < bRightLead ? -aRightLead : bRightLead;
                double correctionY = aBottomLead < bBottomLead ? -aBottomLead : bBottomLead;
                if (Math.abs(correctionX) > Math.abs(correctionY)) {
                    offset.y += correctionY;
                } else {
                    offset.x += correctionX;
                }
            }
            return true;
        }
        return false;
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.farm.game.logic;

import com.mush.farm.game.Game;
import com.mush.farm.game.GameEventQueue;
import com.mush.farm.game.events.BodyEvent;
import com.mush.farm.game.events.CreatureEvent;
import com.mush.farm.game.events.InteractionEvent;
import com.mush.farm.game.logic.ai.CreatureAi;
import com.mush.farm.game.model.Body;
import com.mush.farm.game.model.BodyType;
import com.mush.farm.game.model.Creature;
import com.mush.farm.game.model.MapObject;
import com.mush.farm.game.model.MapObjectType;
import java.awt.Point;
import java.awt.geom.Point2D;

/**
 *
 * @author mush
 */
public class GameEventLogic {

    public static final double SELECT_DISTANCE = GameSizes.TILE_SIZE * 2;
    public static final double PICK_UP_DISTANCE = GameSizes.TILE_SIZE * 1;

    private final Game game;

    public GameEventLogic(Game game) {
        this.game = game;
    }

    public void onEvent(CreatureEvent.Select event) {
        Creature creature = game.creatures.getCreature(event.creatureId);
        if (creature == null) {
            return;
        }

        if (event.select) {
            Body nearest = game.bodies.getClosestBodyTo(creature.body, SELECT_DISTANCE);
            if (nearest != null) {
                creature.select(nearest);
            }
        } else {
            creature.unSelect();
        }
    }

    public void onEvent(CreatureEvent.Interact event) {
        Creature creature = game.creatures.getCreature(event.creatureId);
        if (creature == null) {
            return;
        }

        Body tool = creature.getEquipped();

        if (tool == null) {
            tool = creature.body;
        }

        if (creature.hasSelected()) {
            Body selected = game.bodies.getBody(creature.selectedBodyId);
            if (selected != null && game.bodies.getDistanceBetween(creature.body, selected) < SELECT_DISTANCE) {
                GameEventQueue.send(new InteractionEvent.BodyOnBody(creature, tool, selected));
            }
        } else if (!creature.collisionTilePoints.isEmpty()) {
            for (Point tilePoint : creature.collisionTilePoints) {
                MapObject mapObject = game.gameMap.getMapObject(tilePoint.x, tilePoint.y);
                if (mapObject != null) {
                    GameEventQueue.send(new InteractionEvent.BodyOnMapObject(creature, tool, mapObject));
                }
            }
        } else {
            Point mapPoint = GameSizes.getTileCoordinates(game.getPlayer().body.getPosition());

            MapObject mapObject = game.gameMap.getMapObject(mapPoint.x, mapPoint.y);
            GameEventQueue.send(new InteractionEvent.BodyOnMapObject(creature, tool, mapObject));
        }
    }

    public void onEvent(CreatureEvent.Give event) {
        Creature creature = game.creatures.getCreature(event.creatureId);
        if (creature == null) {
            return;
        }

        if (creature.hasEquipped() && creature.hasSelected()) {
            Body selected = game.bodies.getBody(creature.selectedBodyId);

            if (selected != null && selected.creature != null && game.bodies.getDistanceBetween(creature.body, selected) < SELECT_DISTANCE) {
                Creature peer = selected.creature;

                Body item = creature.unequipDirectly();

                boolean peerHasEquippedItem = peer.hasEquipped()
                        ? false
                        : peer.equipDirectly(item);

                if (!peerHasEquippedItem) {
                    peer.addToInventory(item);
                }

                if (peer.hasSelected() && peer.selectedBodyId.equals(item.bodyId)) {
                    peer.unSelect();
                }
            }
        }
    }

    public void onEvent(CreatureEvent.PickUp event) {
        Creature creature = game.creatures.getCreature(event.creatureId);
        if (creature == null) {
            return;
        }

        Body nearest = game.bodies.getClosestBodyTo(creature.body, PICK_UP_DISTANCE);

        if (nearest != null) {
            if (creature.hasSelected() && creature.selectedBodyId.equals(nearest.bodyId)) {
                creature.unSelect();
            }

            game.bodies.removeFromFreeBodies(nearest);

            boolean hasEquippedNearest = creature.hasEquipped()
                    ? false
                    : creature.equipDirectly(nearest);

            if (!hasEquippedNearest) {
                creature.addToInventory(nearest);
            }
        }
    }

    public void onEvent(CreatureEvent.Drop event) {
        Creature creature = game.creatures.getCreature(event.creatureId);
        if (creature == null) {
            return;
        }
        dropOneItem(creature);
    }

    public void onEvent(CreatureEvent.Equip event) {
        Creature creature = game.creatures.getCreature(event.creatureId);
        if (creature == null) {
            return;
        }
        creature.equipFromInventory(event.inventoryIndex);
    }

    public void onEvent(CreatureEvent.Unequip event) {
        Creature creature = game.creatures.getCreature(event.creatureId);
        if (creature == null) {
            return;
        }
        creature.unequipIntoInventory();
    }

    public void onEvent(CreatureEvent.CycleInventory event) {
        Creature creature = game.creatures.getCreature(event.creatureId);
        if (creature == null) {
            return;
        }
        creature.cycleInventory();
    }

    public void onEvent(CreatureEvent.BodyCollision event) {
        Creature creature = game.creatures.getCreature(event.creatureId);
        Body body = game.bodies.getBody(event.bodyId);
//        System.out.println("Collision YO! " + creature.body.type + " with " + body.type);
    }

    public void onEvent(CreatureEvent.MapCollision event) {
        Creature creature = game.creatures.getCreature(event.creatureId);
        MapObjectType type = event.type;
//        System.out.println("Collision YO! " + creature.body.type + " with " + type);
    }

    public void onEvent(CreatureEvent.Die event) {
        Creature creature = game.creatures.getCreature(event.creatureId);
        if (creature == null) {
            return;
        }
        dropAllItems(creature);
        creature.die();
        game.creatures.unspawn(event.creatureId);
    }

    public void onEvent(CreatureEvent.Reproduce event) {
        Creature parentCreature = game.creatures.getCreature(event.creatureId);
        if (parentCreature == null) {
            return;
        }

        Point2D.Double position = parentCreature.body.getPosition();
        Creature creature = game.creatures.spawn(position.x, position.y + GameSizes.TILE_SIZE / 2, parentCreature.body.type);
        new CreatureAi(game).assignToCreature(creature);
    }

    private boolean dropOneItem(Creature creature) {
        Body item = creature.removeLastFromInventory();
        if (item == null) {
            item = creature.unequipDirectly();
        }
        if (item != null) {
            Point2D.Double position = creature.body.getPosition();
            // just a tiny bit in front of the creature
            game.bodies.addToFreeBodies(item, position.x, position.y + GameSizes.TILE_SIZE / 4);
            return true;
        }
        return false;
    }

    private void dropAllItems(Creature creature) {
        boolean dropped;
        do {
            dropped = dropOneItem(creature);
        } while (dropped);
    }

}

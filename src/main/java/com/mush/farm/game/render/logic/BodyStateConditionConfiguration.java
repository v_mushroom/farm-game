/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.farm.game.render.logic;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.mush.farm.game.model.BodyState;
import com.mush.farm.game.render.logic.BodyStateCondition.Always;
import com.mush.farm.game.render.logic.BodyStateCondition.Between;
import com.mush.farm.game.render.logic.BodyStateCondition.EqualTo;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author mush
 */
public class BodyStateConditionConfiguration {

    public static class Condition {

        public String animationKey;

        @JsonInclude(JsonInclude.Include.NON_NULL)
        public Object equalTo;

        @JsonInclude(JsonInclude.Include.NON_NULL)
        public double[] between;
    }

    @JsonProperty(value = "animationKey")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public String defaultAnimationKey;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public BodyState state;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public List<Condition> conditions;

    public static BodyStateConditionConfiguration createFrom(BodyStateCondition fromBodyStateCondition) {
        BodyStateConditionConfiguration config = new BodyStateConditionConfiguration();
        config.state = fromBodyStateCondition.state;
        for (BodyStateCondition.Condition fromCondition : fromBodyStateCondition.conditions) {
            if (fromCondition instanceof BodyStateCondition.Always) {
                config.defaultAnimationKey = ((Always) fromCondition).animationKey;
            } else if (fromCondition instanceof BodyStateCondition.EqualTo) {
                Condition condition = new Condition();
                condition.animationKey = ((EqualTo) fromCondition).animationKey;
                condition.equalTo = ((EqualTo) fromCondition).value;
                if (config.conditions == null) {
                    config.conditions = new ArrayList<>();
                }
                config.conditions.add(condition);
            } else if (fromCondition instanceof BodyStateCondition.Between) {
                Condition condition = new Condition();
                condition.animationKey = ((Between) fromCondition).animationKey;
                condition.between = new double[]{
                    ((Between) fromCondition).fromValue,
                    ((Between) fromCondition).toValue};
                if (config.conditions == null) {
                    config.conditions = new ArrayList<>();
                }
                config.conditions.add(condition);
            }
        }
        return config;
    }

    public static BodyStateCondition createFrom(BodyStateConditionConfiguration config) {
        BodyStateCondition bodyStateCondition = new BodyStateCondition(config.state);
        if (config.conditions == null) {
            bodyStateCondition.conditions.add(new Always(config.defaultAnimationKey));
        } else {
            for (Condition conditionConfig : config.conditions) {
                if (conditionConfig.equalTo != null) {
                    bodyStateCondition.conditions.add(new EqualTo(
                            conditionConfig.equalTo,
                            conditionConfig.animationKey));
                } else if (conditionConfig.between != null) {
                    bodyStateCondition.conditions.add(new Between(
                            conditionConfig.between[0],
                            conditionConfig.between[1],
                            conditionConfig.animationKey));
                } else {
                    bodyStateCondition.conditions.add(new Always(null));
                }
            }
        }
        return bodyStateCondition;
    }

}

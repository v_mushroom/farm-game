/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.farm.game.render;

import com.mush.farm.game.Game;
import com.mush.farm.game.logic.GameSizes;
import static com.mush.farm.game.logic.GameSizes.TILE_SIZE;
import com.mush.farm.game.logic.path.GraphNode;
import com.mush.farm.game.model.Body;
import com.mush.farm.game.model.GameMap;
import com.mush.farm.game.model.MapObject;
import com.mush.farm.game.model.MapWater;
import com.mush.farm.game.model.SegmentedValues;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author mush
 */
public class GameRenderer {

    public static final int TILE_ZOOM = 2;

    // For now same as map
    public static final int VIEW_WIDTH = GameMap.MAP_WIDTH;
    public static final int VIEW_HEIGHT = GameMap.MAP_HEIGHT;

    private Game game;
//    private GameMap gameMap;
//    private ImageLibrary images;
    //private BodyRenderLogic bodyLogic;
//    private RenderBodies renderBodies;
    private final BodyDrawComparator bodyDrawComparator;
    private List<Body>[] bodiesByLine;
    private BufferedImage screenImage;
    private Graphics2D screenGraphics;

    public GameRenderer(Game game) {
        this.game = game;
//        this.gameMap = game.gameMap;

        bodyDrawComparator = new BodyDrawComparator();
        bodiesByLine = new List[VIEW_HEIGHT + 1];
        for (int i = 0; i < bodiesByLine.length; i++) {
            bodiesByLine[i] = new ArrayList<>();
        }

//        this.images = new ImageLibrary();
////        this.bodyLogic = new BodyRenderLogic(images);
//        this.renderBodies = new RenderBodies(game, new BodyRenderLogic(images));
        createScreenImage();
    }

    private void createScreenImage() {
        if (screenGraphics != null) {
            screenGraphics.dispose();
            screenGraphics = null;
            screenImage = null;
        }

        screenImage = new BufferedImage(
                VIEW_WIDTH * GameSizes.TILE_SIZE,
                VIEW_HEIGHT * GameSizes.TILE_SIZE,
                BufferedImage.TYPE_INT_RGB);
        screenGraphics = screenImage.createGraphics();
    }

    public void render(Graphics2D g) {
        renderTiles(screenGraphics);

        renderBodiesByLines(screenGraphics, game.bodies.getBodies());
        
        renderGraphTest(screenGraphics);

        AffineTransform t = g.getTransform();
        g.scale(TILE_ZOOM, TILE_ZOOM);

        g.drawImage(screenImage, 0, 0, null);

        g.setTransform(t);
    }

    private void renderTiles(Graphics2D g) {
        GameMap gameMap = game.gameMap;
        for (int j = 0; j < VIEW_HEIGHT; j++) {
            for (int i = 0; i < VIEW_WIDTH; i++) {
                MapObject object = gameMap.getMapObject(i, j);
                if (object != null) {
                    renderTile(g, i, j, object);

                    if (game.getShowStats()) {
                        MapWater water = gameMap.getWaterObject(i, j);
                        renderTileStats(g, i, j, object, water);
                    }
                }
            }
        }
    }

    private void renderBodies(Graphics2D g, List<Body> allBodies) {
        List<Body> depthSortedBodies = depthSortBodies(game.bodies.getBodies());

        for (Body body : depthSortedBodies) {
            renderBody(g, body);
        }
    }

    private void renderBodiesByLines(Graphics2D g, List<Body> allBodies) {
        splitBodiesIntoLines(allBodies);
        depthSortBodiesInLines();

        for (int j = 0; j < bodiesByLine.length; j++) {
//            for (int i = 0; i < VIEW_HEIGHT; i++) {
//                MapObject object = gameMap.getMapObject(i, j);
//                if (object != null && object.type == MapObjectType.STONE_WALL) {
//                    BufferedImage tile = tileMap.get(MapObjectType.STONE_WALL_TOP);
//                    int x = i * TILE_SIZE;
//                    int y = j * TILE_SIZE;
//                    if (tile != null) {
//                        g.drawImage(tile, x, y - TILE_SIZE, null);
//                    }
////                    tile = tileMap.get(MapObjectType.STONE_WALL);
////                    if (tile != null) {
////                        g.drawImage(tile, x, y, null);
////                    }
//                }
//            }
            for (Body body : bodiesByLine[j]) {
                renderBody(g, body);
            }
        }

        clearBodyLines();
    }
    
    private void renderGraphTest(Graphics2D g) {
        g.setColor(Color.RED);
        for (GraphNode n : game.testGraph.nodes.values()) {
            int x = (int) n.position.x;
            int y = (int) n.position.y;
            for (Integer n2id : n.connections.keySet()) {
                GraphNode n2 = game.testGraph.nodes.get(n2id);
                int x2 = (int) n2.position.x;
                int y2 = (int) n2.position.y;
                g.drawLine(x, y, x2, y2);
            }
        }
        g.setColor(Color.YELLOW);
        for (GraphNode n : game.testGraph.nodes.values()) {
            int x = (int) n.position.x;
            int y = (int) n.position.y;
            g.drawOval(x - 2, y - 2, 4, 4);
        }
        
        g.setColor(Color.GREEN);
        if (game.testPath != null) {
            Point2D.Double prevP = null;
            for (GraphNode n : game.testPath) {
                Point2D.Double p = n.position;
                if (prevP != null) {
                    g.drawLine((int)p.x, (int)p.y, (int)prevP.x, (int)prevP.y);
                }
                prevP = p;
            }
        }
    }

    // TODO: got a ConcurrentModificationException, check it
    private void splitBodiesIntoLines(List<Body> allBodies) {
        for (Body body : allBodies) {
            int v = (int) (body.position.y / TILE_SIZE);
            if (v >= 0 && v < bodiesByLine.length) {
                bodiesByLine[v].add(body);
            }
        }
    }

    private void clearBodyLines() {
        for (List<Body> lineList : bodiesByLine) {
            lineList.clear();
        }
    }

    private void depthSortBodiesInLines() {
        for (int v = 0; v < bodiesByLine.length; v++) {
            Collections.sort(bodiesByLine[v], bodyDrawComparator);
        }
    }

    private List<Body> depthSortBodies(List<Body> original) {
        List<Body> ordered = new ArrayList<>(original);

        Collections.sort(ordered, bodyDrawComparator);

        return ordered;
    }

    private void renderBody(Graphics2D g, Body body) {
        int x = (int) Math.round(body.position.x);
        int y = (int) Math.round(body.position.y);

        BodyRepresentation renderBody = game.renderBodies.getRepresentation(body);

        if (body.creature != null && body.creature.hasSelected()) {
            Body selected = game.bodies.getBody(body.creature.selectedBodyId);
            if (selected != null) {
                int x2 = (int) Math.round(selected.position.x);
                int y2 = (int) Math.round(selected.position.y);
                g.setColor(Color.YELLOW);
                g.drawLine(x, y, x2, y2);
            }
        }

        if (!body.containedBodies.isEmpty()) {
            for (int i = body.containedBodies.size() - 1; i >= 0; i--) {
                Body subBody = body.containedBodies.get(i);
                BodyRepresentation renderSubBody = game.renderBodies.getRepresentation(subBody);
                g.drawImage(game.images.animations.getFrame(renderSubBody.getAnimation(), renderSubBody.animationFrame),
                        x + i % 2 - TILE_SIZE / 2,
                        y - TILE_SIZE * 2 * 3 / 4 - i * 3,
                        null);
            }
        }

        g.drawImage(game.images.animations.getFrame(renderBody.getAnimation(), renderBody.animationFrame),
                x - TILE_SIZE / 2,
                y - TILE_SIZE,
                null);

        if (body.creature != null) {
            Body equippedBody = body.creature.getEquipped();
            if (equippedBody != null) {
                BodyRepresentation renderEquippedBody = game.renderBodies.getRepresentation(equippedBody);
                g.drawImage(game.images.animations.getFrame(renderEquippedBody.getAnimation(), renderEquippedBody.animationFrame),
                        x,
                        y - TILE_SIZE - TILE_SIZE / 10,
                        null);
            }

            if (game.getShowVectors()) {
                if (body.creature.creatureAi != null) {
                    Point2D.Double wp = body.creature.creatureAi.navigator.getWayPoint();
                    int x2 = (int) Math.round(wp.x);
                    int y2 = (int) Math.round(wp.y);
                    g.setColor(Color.GREEN);
                    g.drawLine(x, y, x2, y2);

                }

                Point2D.Double dir = body.creature.movementDirection;
                int x2 = x + (int) Math.round(dir.x * TILE_SIZE);
                int y2 = y + (int) Math.round(dir.y * TILE_SIZE);
                g.setColor(Color.RED);
                g.drawLine(x, y, x2, y2);
            }
        }

        if (game.getShowStats()) {
            renderBodyStats(g, x - TILE_SIZE / 2, y - TILE_SIZE, body);
        }
    }

    private void renderTile(Graphics2D g, int u, int v, MapObject object) {
        BufferedImage tile = game.images.get(object.type);
        int x = u * TILE_SIZE;
        int y = v * TILE_SIZE;
        if (tile != null) {
            g.drawImage(tile, x, y, null);
        }
    }

    private void renderTileStats(Graphics2D g, int u, int v, MapObject object, MapWater water) {
        int x = u * TILE_SIZE;
        int y = v * TILE_SIZE;

        g.setColor(Color.BLUE);
        int waterLenght = (int) (water.getValue() * (TILE_SIZE - 2));
        g.drawLine(x + 1, y + 3, x + 1 + waterLenght, y + 3);

        g.setColor(new Color(0, 0.2f, 1, 0.2f));
        if (water.getValue() > 0) {
            g.fillRect(x + 1, y + 1, TILE_SIZE - 2, TILE_SIZE - 2);
        }

        g.setColor(Color.YELLOW);
        int ageLength = (int) (object.getAgePercent() * (TILE_SIZE - 2));
        g.drawLine(x + 1, y + 1, x + 1 + ageLength, y + 1);

        g.setColor(Color.RED);
        int healthLength = (int) (object.integrity * (TILE_SIZE - 2));
        g.drawLine(x + 1, y + 2, x + 1 + healthLength, y + 2);
    }

    private void renderBodyStats(Graphics2D g, int x, int y, Body body) {
        g.setColor(Color.GREEN);
        if (body.healFactor != null) {
            renderSegmentedValues(g, x, y + TILE_SIZE / 2, body.healFactor, TILE_SIZE / 2);
        }
        g.setColor(Color.RED);
        if (body.hurtFactor != null) {
            renderSegmentedValues(g, x, y + TILE_SIZE / 2, body.hurtFactor, TILE_SIZE / 2);
        }

        g.setColor(Color.YELLOW);
        int ageLength = (int) (body.getAgePercent() * (TILE_SIZE - 2));
        g.drawLine(x + 1, y + 1, x + 1 + ageLength, y + 1);

        if (body.actualBox != null) {
            g.setColor(Color.BLUE);
            g.drawRect((int) body.actualBox.left, (int) body.actualBox.top,
                    (int) (body.actualBox.right - body.actualBox.left), (int) (body.actualBox.bottom - body.actualBox.top));
        }

        g.setColor(Color.RED);
        int healthLength = (int) (body.integrity * (TILE_SIZE - 2));
        g.drawLine(x + 1, y + TILE_SIZE, x + 1 + healthLength, y + TILE_SIZE);
    }

    private void renderSegmentedValues(Graphics2D g, int x, int y, SegmentedValues values, int height) {
        int u0 = x;
        int v0 = (int) (y - values.evaluate(0) * height);
        for (int i = 1; i < 20; i++) {
            double t = i / 20.0;
            int u = (int) (x + (TILE_SIZE * t));
            int v = (int) (y - values.evaluate(t) * height);
            g.drawLine(u0, v0, u, v);
            u0 = u;
            v0 = v;
        }
    }

}

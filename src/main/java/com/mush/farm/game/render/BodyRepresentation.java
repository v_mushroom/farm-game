/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.farm.game.render;

import com.mush.farm.game.model.Body;
import com.mush.farm.game.model.BodyType;
import java.util.Objects;

/**
 *
 * @author mush
 */
public class BodyRepresentation {

    public final int bodyId;
    public BodyType bodyType;

    private Integer animationNameIndex;
    public int animationFrame;
    public double timeSinceAnimationFrame;

    public BodyRepresentation(Body body) {
        this.bodyId = body.bodyId;
        this.bodyType = body.type;
        animationNameIndex = null;
        resetFrame();
    }

    public void update(double seconds) {
        timeSinceAnimationFrame += seconds;
    }

    public void setAnimation(Integer animationNameIndex) {
        if (!Objects.equals(this.animationNameIndex, animationNameIndex)) {
            this.animationNameIndex = animationNameIndex;
            resetFrame();
        }
    }

    public Integer getAnimation() {
        return animationNameIndex;
    }

    public void resetFrame() {
        animationFrame = 0;
        timeSinceAnimationFrame = 0;
    }

}

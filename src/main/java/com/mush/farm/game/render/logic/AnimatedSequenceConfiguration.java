/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.farm.game.render.logic;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author mush
 */
public class AnimatedSequenceConfiguration {

    public static class FrameConfiguration {

        public int u;
        public int v;
        public double duration;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public String spriteSheet;
    
    @JsonIgnore
    public List<FrameConfiguration> frames;
    
    @JsonProperty(value = "frames")
    public List<double[]> jsonFrames;
    
    public void useJsonFrames() {
        frames = new ArrayList<>();
        for (double[] jsonFrame : jsonFrames) {
            FrameConfiguration frame = new FrameConfiguration();
            frame.u = (int) jsonFrame[0];
            frame.v = (int) jsonFrame[1];
            frame.duration = jsonFrame.length > 2 ? jsonFrame[2] : 0;
            frames.add(frame);
        }
    }

    public AnimatedSequenceConfiguration setSpriteSheet(String spriteSheet) {
        this.spriteSheet = spriteSheet;
        return this;
    }

    public AnimatedSequenceConfiguration addFrame(int u, int v) {
        return addFrame(u, v, 0);
    }

    public AnimatedSequenceConfiguration addFrame(int u, int v, double d) {
        FrameConfiguration frame = new FrameConfiguration();
        frame.u = u;
        frame.v = v;
        frame.duration = d;
        if (frames == null) {
            frames = new ArrayList<>();
        }
        frames.add(frame);
        return this;
    }

}

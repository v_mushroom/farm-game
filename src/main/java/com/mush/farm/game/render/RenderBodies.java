/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.farm.game.render;

import com.mush.farm.game.Game;
import com.mush.farm.game.events.BodyEvent;
import com.mush.farm.game.model.Body;
import com.mush.farm.game.model.BodyState;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author mush
 */
public class RenderBodies {

    public Map<Integer, BodyRepresentation> bodyIndex;
    private final Game game;

    public RenderBodies(Game game) {
        this.game = game;
        bodyIndex = new HashMap<>();
    }

    public void update(double seconds) {
        // drive animations
        for (BodyRepresentation representation : bodyIndex.values()) {
            representation.update(seconds);

            AnimatedSequence animation
                    = game.images.animations.getAnimation(representation.getAnimation());

            if (animation != null) {
                animation.update(representation);
            }
        }
    }

    public void onEvent(BodyEvent.Remove event) {
        bodyIndex.remove(event.bodyId);
    }

    public void onEvent(BodyEvent.ChangeState event) {
        Body body = game.bodies.getBody(event.bodyId);
        BodyRepresentation representation = bodyIndex.get(body.bodyId);
        if (representation != null) {
            onChangeState(body, event.stateKey, representation);
        }
    }

    public void onEvent(BodyEvent.ChangeType event) {
//        System.out.println("on change type " + event.type.name());
        Body body = game.bodies.getBody(event.bodyId);
        BodyRepresentation representation = bodyIndex.get(event.bodyId);
        if (representation != null) {
            representation.bodyType = body.type;

            String animationName = game.bodyAnimationLogic.getAnimationName(body, null);
            representation.setAnimation(game.images.animations.getAnimationIndex(animationName));

            applyStates(body, representation);
        }
    }

    /**
     * Always returns representation, creates new one if not exists.
     *
     * @param body
     * @return
     */
    public BodyRepresentation getRepresentation(Body body) {
        BodyRepresentation representation = bodyIndex.get(body.bodyId);
        if (representation == null) {
            representation = new BodyRepresentation(body);
            applyStates(body, representation);
            bodyIndex.put(body.bodyId, representation);
        }
        return representation;
    }

    private void onChangeState(Body body, BodyState stateKey, BodyRepresentation representation) {
//        System.out.println("change state " + body.bodyId + " : " + stateKey);

        if (game.bodyAnimationLogic != null) {
            String animationName = game.bodyAnimationLogic.getAnimationName(body, stateKey);
//            System.out.println(animationName);
//            System.out.println(body.getStateMap());
            if (animationName != null) {
                representation.setAnimation(game.images.animations.getAnimationIndex(animationName));
            }
        }
    }

    private void applyStates(Body body, BodyRepresentation representation) {
        Set<BodyState> states = body.getStates();
        for (BodyState state : states) {
            onChangeState(body, state, representation);
        }
        onChangeState(body, BodyState.AGE, representation);
    }

}

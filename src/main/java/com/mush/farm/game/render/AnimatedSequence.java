/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.farm.game.render;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author mush
 */
public class AnimatedSequence {

    private List<BufferedImage> frameList;
    private List<Double> frameDurationList;

    public AnimatedSequence() {
        frameList = new ArrayList<>();
        frameDurationList = new ArrayList<>();
    }

    public void addFrame(BufferedImage frame, double duration) {
        frameList.add(frame);
        frameDurationList.add(duration);
    }

    public BufferedImage getFrame(int index) {
        if (index < frameList.size()) {
            return frameList.get(index);
        } else {
            return null;
        }
    }

    public void update(BodyRepresentation representation) {
        // magic
//        representation.animationFrame++;
//        if (representation.animationFrame >= frameList.size()) {
//            representation.animationFrame = 0;
//        }
        advanceFrames(representation);
    }
    
    private void advanceFrames(BodyRepresentation representation) {
        while(advanceFrame(representation)) {
        }
    }

    private boolean advanceFrame(BodyRepresentation representation) {
        double frameDuration = frameDurationList.get(representation.animationFrame);
        if (frameDuration <= 0) {
            return false;
        }
        if (representation.timeSinceAnimationFrame >= frameDuration) {
            representation.animationFrame++;
            if (representation.animationFrame >= frameList.size()) {
                representation.animationFrame = 0;
            }
            representation.timeSinceAnimationFrame -= frameDuration;
            return true;
        }
        return false;
    }

}

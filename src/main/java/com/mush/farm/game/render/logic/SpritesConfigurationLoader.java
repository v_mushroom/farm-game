/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.farm.game.render.logic;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.mush.farm.game.model.BodyState;
import com.mush.farm.game.model.BodyType;
import com.mush.farm.game.render.AnimatedSequence;
import com.mush.farm.game.render.AnimationLibrary;
import com.mush.farm.game.render.SpriteSheet;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author mush
 */
public class SpritesConfigurationLoader {

    public static class JsonConfiguration {

        public String spriteSheet;
        public Map<String, AnimatedSequenceConfiguration> animations;
        public Map<BodyType, List<BodyStateConditionConfiguration>> bodyConditions;
    }

    private JsonConfiguration config;
    private Map<BodyType, List<BodyStateCondition>> bodyConditions;

    private AnimationLibrary animations;
    private BodyStateAnimationLogic spriteLogic;

    public SpritesConfigurationLoader(AnimationLibrary animations, BodyStateAnimationLogic spriteLogic) {
        this.animations = animations;
        this.spriteLogic = spriteLogic;
    }

    public void load() throws IOException {
        bodyConditions = new HashMap<>();

        config = loadJson("img/sprites.json");

//        testToJson();

        processConfiguration();

        spriteLogic.bodyConditions = bodyConditions;
    }

    private void processConfiguration() throws IOException {

        for (Map.Entry<String, AnimatedSequenceConfiguration> e : config.animations.entrySet()) {
            AnimatedSequenceConfiguration animation = e.getValue();
            if (animation.spriteSheet == null) {
                animation.spriteSheet = config.spriteSheet;
            }
            animation.useJsonFrames();
        }
        
        for (Map.Entry<BodyType, List<BodyStateConditionConfiguration>> e : config.bodyConditions.entrySet()) {
            List<BodyStateCondition> conditions = new ArrayList<>();
            
            for (BodyStateConditionConfiguration conditionConfig : e.getValue()) {
                conditions.add(BodyStateConditionConfiguration.createFrom(conditionConfig));
            }
            
            bodyConditions.put(e.getKey(), conditions);
        }

        makeAnimations(config.animations, new SpriteSheet("img/sprites.png"));
    }

    private JsonConfiguration loadJson(String fileName) throws IOException {
        File jsonFile = new File(fileName);
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        JsonConfiguration configuration = mapper.readValue(jsonFile, JsonConfiguration.class);
        return configuration;
    }

    private void testToJson() throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.INDENT_OUTPUT, true);

        /*
        for (Map.Entry<BodyType, List<BodyStateCondition>> e : bodyConditions.entrySet()) {
            List<BodyStateConditionConfiguration> jsonConditions = new ArrayList<>();
            for (BodyStateCondition condition : e.getValue()) {
                BodyStateConditionConfiguration jsonCondition = BodyStateConditionConfiguration.createFrom(condition);
                jsonConditions.add(jsonCondition);
            }
            config.bodyConditions.put(e.getKey(), jsonConditions);
        }
        */

        System.out.println(mapper.writeValueAsString(bodyConditions));
        System.out.println(mapper.writeValueAsString(config));
    }

    public void makeAnimations(Map<String, AnimatedSequenceConfiguration> animationsConfig, SpriteSheet spriteSheet) {
        for (Map.Entry<String, AnimatedSequenceConfiguration> item : animationsConfig.entrySet()) {
            animations.addAnimation(item.getKey(), makeAnimation(item.getValue(), spriteSheet));
        }
    }

    private AnimatedSequence makeAnimation(AnimatedSequenceConfiguration config, SpriteSheet spriteSheet) {
        AnimatedSequence sequence = new AnimatedSequence();
        for (AnimatedSequenceConfiguration.FrameConfiguration frameConfig : config.frames) {
            sequence.addFrame(spriteSheet.cutImage(frameConfig.u, frameConfig.v), frameConfig.duration);
        }
        return sequence;
    }

    private void addAnimation(String name, AnimatedSequenceConfiguration animation) {
        config.animations.put(name, animation);
    }

    private BodyStateCondition getCondition(BodyType type, BodyState state) {
        List<BodyStateCondition> typeConditionList = bodyConditions.get(type);
        if (typeConditionList == null) {
            typeConditionList = new ArrayList<>();
            bodyConditions.put(type, typeConditionList);
        }
        BodyStateCondition condition = null;
        for (BodyStateCondition aCondition : typeConditionList) {
            if (aCondition.state == state) {
                condition = aCondition;
                break;
            }
        }

        if (condition == null) {
            condition = new BodyStateCondition(state);
            typeConditionList.add(condition);
        }

        return condition;
    }

    private BodyStateCondition createConditionAlways(BodyType type, BodyState state, String key) {
        BodyStateCondition condition = getCondition(type, state);

        condition.conditions.add(new BodyStateCondition.Always(key));
        return condition;
    }

    private BodyStateCondition createConditionEqual(BodyType type, BodyState state, Object value, String key) {
        BodyStateCondition condition = getCondition(type, state);

        condition.state = state;
        condition.conditions.add(new BodyStateCondition.EqualTo(value, key));
        return condition;
    }

    private BodyStateCondition createConditionBetween(BodyType type, BodyState state, double low, double high, String key) {
        BodyStateCondition condition = getCondition(type, state);

        condition.state = state;
        condition.conditions.add(new BodyStateCondition.Between(low, high, key));
        return condition;
    }

}

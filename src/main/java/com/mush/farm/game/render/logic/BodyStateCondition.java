/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.farm.game.render.logic;

import com.mush.farm.game.model.BodyState;
import com.mush.farm.game.model.BodyType;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 *
 * @author mush
 */
public class BodyStateCondition {

    public static abstract class Condition {

        public final String animationKey;

        public Condition(String key) {
            this.animationKey = key;
        }

        public abstract boolean evaluate(Object value);
    }

    public static class Always extends Condition {

        public Always(String key) {
            super(key);
        }

        @Override
        public boolean evaluate(Object value) {
            return true;
        }
    }

    public static class EqualTo extends Condition {

        public Object value;

        public EqualTo(Object v, String key) {
            super(key);
            this.value = v;
        }

        @Override
        public boolean evaluate(Object value) {
            return Objects.equals(this.value, value);
        }
    }

    public static class Between extends Condition {

        public double fromValue;
        public double toValue;

        public Between(double low, double high, String key) {
            super(key);
            this.fromValue = low;
            this.toValue = high;
        }

        @Override
        public boolean evaluate(Object value) {
            if (value instanceof Number) {
                double number = ((Number) value).doubleValue();
                return number >= fromValue && number <= toValue;
            }
            return false;
        }
    }

    public BodyState state;
    public List<Condition> conditions;

    public BodyStateCondition(BodyState state) {
        this.state = state;
        this.conditions = new ArrayList<>();
    }

    private String evaluate(Object value) {
        for (Condition condition : conditions) {
            if (condition.evaluate(value)) {
                return condition.animationKey;
            }
        }
        return null;
    }

    public String evaluate(Map<BodyState, Object> stateMap) {
        Object value = this.state != null ? stateMap.get(this.state) : null;
        return evaluate(value);
    }

    public static String evaluate(BodyType type, Map<BodyState, Object> stateMap, List<BodyStateCondition> list) {
        StringBuilder sb = new StringBuilder(type.name());

        for (BodyStateCondition stateCondition : list) {
            String key = stateCondition.evaluate(stateMap);
            if (key != null) {
                sb.append("_").append(key);
            }
        }

        return sb.toString();
    }

}

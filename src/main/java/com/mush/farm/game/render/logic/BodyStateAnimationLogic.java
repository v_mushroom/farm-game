/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.farm.game.render.logic;

import com.mush.farm.game.model.Body;
import com.mush.farm.game.model.BodyState;
import com.mush.farm.game.model.BodyType;
import java.util.List;
import java.util.Map;

/**
 *
 * @author mush
 */
public class BodyStateAnimationLogic {

    public Map<BodyType, List<BodyStateCondition>> bodyConditions;

    public BodyStateAnimationLogic() {
    }

    public String getAnimationName(Body body, BodyState changedBodyState) {
        return getAnimationName(body.type, body.getStateMap());
    }

    private String getAnimationName(BodyType type, Map<BodyState, Object> stateMap) {
        List<BodyStateCondition> conditions = bodyConditions.get(type);
        return BodyStateCondition.evaluate(type, stateMap, conditions);
    }

}

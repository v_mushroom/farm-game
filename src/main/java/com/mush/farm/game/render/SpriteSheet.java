/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.farm.game.render;

import static com.mush.farm.game.logic.GameSizes.TILE_SIZE;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;

/**
 *
 * @author mush
 */
public class SpriteSheet {

    private BufferedImage image;
    // TODO: optimize multiple cuts of same sprite

    public SpriteSheet(String fileName) throws IOException {
        image = ImageIO.read(new File(fileName));
    }

    public BufferedImage cutImage(int u, int v) {
        BufferedImage tile = new BufferedImage(TILE_SIZE, TILE_SIZE, image.getType());
        Graphics2D g = tile.createGraphics();
        g.drawImage(image, -u * TILE_SIZE, -v * TILE_SIZE, null);
        g.dispose();
        return tile;
    }

}

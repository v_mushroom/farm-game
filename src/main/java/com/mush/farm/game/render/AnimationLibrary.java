/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.farm.game.render;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author mush
 */
public class AnimationLibrary {

    private List<AnimatedSequence> animations;
    private Map<String, Integer> animationIndexByKey;

    public AnimationLibrary() {
        animations = new ArrayList<>();
        animationIndexByKey = new HashMap<>();
    }

    public void addAnimation(String animationName, AnimatedSequence animation) {
        int index = animations.size();
        animations.add(animation);
        animationIndexByKey.put(animationName, index);
    }

    // try not to use
    @Deprecated
    public AnimatedSequence getAnimation(String animationName) {
        return getAnimation(getAnimationIndex(animationName));
    }

    public AnimatedSequence getAnimation(Integer index) {
        if (index == null) {
            return null;
        }
        if (index >= animations.size()) {
            return null;
        }
        return animations.get(index);
    }

    public Integer getAnimationIndex(String animationName) {
        if (animationName == null) {
            return null;
        }
        return animationIndexByKey.get(animationName);
    }

    // try not to use
    @Deprecated
    public BufferedImage getFrame(String animationName, int frame) {
        AnimatedSequence animation = getAnimation(animationName);
        if (animation == null) {
            return null;
        }
        return animation.getFrame(frame);
    }

    public BufferedImage getFrame(Integer animationIndex, int frame) {
        AnimatedSequence animation = getAnimation(animationIndex);
        if (animation == null) {
            return null;
        }
        return animation.getFrame(frame);
    }

}

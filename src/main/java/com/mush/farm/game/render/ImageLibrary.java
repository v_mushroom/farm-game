/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.farm.game.render;

import static com.mush.farm.game.logic.GameSizes.TILE_SIZE;
import com.mush.farm.game.model.MapObjectType;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import javax.imageio.ImageIO;

/**
 *
 * @author mush
 */
public class ImageLibrary {

    private Map<MapObjectType, BufferedImage> tileMap;
    public AnimationLibrary animations;

    public ImageLibrary() {
        loadImages();
        animations = new AnimationLibrary();
    }

    private void loadImages() {
        tileMap = new HashMap<>();

        try {
            loadTiles();

        } catch (IOException e) {

        }
    }

    private void loadTiles() throws IOException {
        BufferedImage tilesImage = ImageIO.read(new File("img/tiles.png"));

        cutTile(tilesImage, 0, 0, MapObjectType.DIRT);
        cutTile(tilesImage, 1, 0, MapObjectType.GRASS);
        cutTile(tilesImage, 2, 0, MapObjectType.WATER);
        cutTile(tilesImage, 3, 0, MapObjectType.DIRT_HOLE);
        cutTile(tilesImage, 3, 1, MapObjectType.DIRT_STOMPED);
        cutTile(tilesImage, 0, 1, MapObjectType.ORGANIC_RUBBLE);
        cutTile(tilesImage, 1, 1, MapObjectType.STONE_RUBBLE);
        cutTile(tilesImage, 4, 1, MapObjectType.STONE_WALL);
        cutTile(tilesImage, 4, 0, MapObjectType.STONE_WALL_TOP);
        cutTile(tilesImage, 0, 2, MapObjectType.POTATO_PLANTED);
        cutTile(tilesImage, 1, 2, MapObjectType.POTATO_SAPLING);
        cutTile(tilesImage, 2, 2, MapObjectType.POTATO_PLANT);
    }

    public BufferedImage cutImage(BufferedImage img, int u, int v) {
        BufferedImage tile = new BufferedImage(TILE_SIZE, TILE_SIZE, img.getType());
        Graphics2D g = tile.createGraphics();
        g.drawImage(img, -u * TILE_SIZE, -v * TILE_SIZE, null);
        g.dispose();
        return tile;
    }

    private void cutTile(BufferedImage img, int u, int v, MapObjectType type) {
        BufferedImage tile = cutImage(img, u, v);
        tileMap.put(type, tile);
    }

    public BufferedImage get(MapObjectType type) {
        return tileMap.get(type);
    }

}

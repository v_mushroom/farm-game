/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.farm.game;

import com.mush.farm.game.logic.GameInteractionsLogic;
import com.mush.farm.game.events.ControlEvent;
import com.mush.farm.game.events.GenericGameEvent;
import com.mush.farm.game.logic.GameEventLogic;
import com.mush.farm.game.logic.GameMapEventLogic;
import com.mush.farm.game.logic.GameSizes;
import com.mush.farm.game.logic.MovementLogic;
import com.mush.farm.game.render.GameRenderer;
import com.mush.farm.game.model.BodyType;
import com.mush.farm.game.model.GameBodies;
import com.mush.farm.game.model.GameCreatures;
import com.mush.farm.game.model.Creature;
import com.mush.farm.game.logic.ai.CreatureAi;
import com.mush.farm.game.logic.path.Graph;
import com.mush.farm.game.logic.path.GraphNode;
import com.mush.farm.game.logic.path.PathFinder;
import com.mush.farm.game.model.GameMap;
import com.mush.farm.game.model.GameMapGenerator;
import com.mush.farm.game.model.MapObjectType;
import com.mush.farm.game.render.ImageLibrary;
import com.mush.farm.game.render.RenderBodies;
import com.mush.farm.game.render.logic.BodyStateAnimationLogic;
import com.mush.farm.game.render.logic.SpritesConfigurationLoader;
import java.awt.Point;
import java.util.List;

/**
 *
 * @author mush
 */
public class Game {

    public GameControl control;
    public GameRenderer renderer;
    public GameMap gameMap;
    public GameKeyboardListener keyboardListener;
    public GameBodies bodies;
    public GameCreatures creatures;
    public ImageLibrary images;
    public RenderBodies renderBodies;
    public BodyStateAnimationLogic bodyAnimationLogic;

    private GameEventLogic gameEventLogic;
    private GameMapEventLogic gameMapLogic;
    private GameInteractionsLogic interactionsLogic;
    public MovementLogic movementLogic;

    private Integer playerCreatureId;
    private boolean showStats;
    private boolean showVectors;
    private boolean paused = false;
    private boolean mapCollisionsEnabled = true;

    public Graph testGraph;
    public PathFinder testPathFinder;
    public List<GraphNode> testPath;
    public double testDelay;

    public Game() {
        control = new GameControl(this);
        bodies = new GameBodies();
        creatures = new GameCreatures(bodies);
        gameMap = new GameMap();
        renderer = new GameRenderer(this);
        keyboardListener = new GameKeyboardListener(control);

        gameEventLogic = new GameEventLogic(this);
        gameMapLogic = new GameMapEventLogic(gameMap, bodies);
        interactionsLogic = new GameInteractionsLogic(this);
        movementLogic = new MovementLogic(this);

        images = new ImageLibrary();
        bodyAnimationLogic = new BodyStateAnimationLogic();
        renderBodies = new RenderBodies(this);

        try {
            SpritesConfigurationLoader loader
                    = new SpritesConfigurationLoader(images.animations, bodyAnimationLogic);
            loader.load();

        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        GameEventQueue.addListener(this);
        GameEventQueue.addListener(gameEventLogic);
        GameEventQueue.addListener(gameMapLogic);
        GameEventQueue.addListener(interactionsLogic);
        GameEventQueue.addListener(movementLogic);
        GameEventQueue.addListener(renderBodies);

        showStats = false;
        showVectors = true;

        new GameMapGenerator(gameMap).generate();
        setupBodies();
    }

    // todo: ugly, put all of this somewhere else
    private void setupBodies() {
        int width = GameMap.MAP_WIDTH * GameSizes.TILE_SIZE;
        int height = GameMap.MAP_HEIGHT * GameSizes.TILE_SIZE;

        for (int i = 0; i < 3; i++) {
            bodies.spawnBody(BodyType.BUCKET, Math.random() * width, Math.random() * height);
        }

        bodies.spawnBody(BodyType.SHOVEL, Math.random() * width, Math.random() * height);

        for (int i = 0; i < 3; i++) {
            bodies.spawnBody(BodyType.FLY, Math.random() * width, Math.random() * height);
        }

        for (int i = 0; i < 3; i++) {
            creatures.spawn(
                    width * (0.25 + Math.random() * 0.5),
                    height * (0.25 + Math.random() * 0.5),
                    BodyType.PERSON);
        }

        for (int i = 0; i < 3; i++) {
            Creature creature = creatures.spawn(
                    width * (0.25 + Math.random() * 0.5),
                    height * (0.25 + Math.random() * 0.5),
                    BodyType.HOG);
            new CreatureAi(this).assignToCreature(creature);
        }

        playerCreatureId = 0;

        setupGraphTest(width, height);
    }

    private void setupGraphTest(int width, int height) {
        testGraph = new Graph();
        int nodes = 10;
        for (int i = 0; i < nodes; i++) {
            for (int j = 0; j < nodes; j++) {
                testGraph.addNode(1.0 * i / nodes * width, 1.0 * j / nodes * height);
            }
        }
        for (int i = 0; i < nodes * 20; i++) {
            int x = (int) (Math.random() * nodes);
            int y = (int) (Math.random() * nodes);

            int a = y + x * nodes;
            int dx = 0;
            int dy = 0;
            int ofs = Math.random() < 0.5 ? 1 : -1;
            if (Math.random() < 0.5) {
                dx = ofs;
                if (x == 0) {
                    dx = 1;
                }
                if (x >= nodes - 1) {
                    dx = -1;
                }
            } else {
                dy = ofs;
                if (y == 0) {
                    dy = 1;
                }
                if (y >= nodes - 1) {
                    dy = -1;
                }
            }
            int b = (y + dy) + (x + dx) * nodes;

            testGraph.connect(a, b);
        }

        testPathFinder = new PathFinder(testGraph);
        findPath();
    }

    public void findPath() {
        int nodes = 10;
        int nodes2 = nodes * nodes;
        testPathFinder.setStartEnd((int) (Math.random() * nodes2), (int) (Math.random() * nodes2));

        testPath = null;
        testPath = testPathFinder.find();
    }

    public void update(double elapsedSeconds) {
        GameEventQueue.processQueue();

        double seconds = paused ? 0 : elapsedSeconds;

        creatures.update(seconds);
        gameMap.update(seconds);
        bodies.update(seconds);
        renderBodies.update(seconds);

        updateGraphTest(elapsedSeconds);
    }

    private void updateGraphTest(double elapsedSeconds) {
        testDelay += elapsedSeconds;
        if (testDelay > 5) {
            testDelay = 0;
            findPath();
        }
    }

    public void togglePause() {
        paused = !paused;
    }

    public void changeCreature() {
        Creature player = getPlayer();
        List<Creature> all = creatures.getCreatures();
        if (player != null) {
            int ind = all.indexOf(player);
            ind++;
            player.movementDirection.setLocation(0, 0);
            playerCreatureId = ind >= all.size() ? 0 : ind;
        } else {
            if (all.isEmpty()) {
                playerCreatureId = 0;
            } else {
                playerCreatureId = all.get(0).creatureId;
            }
        }
    }

    public Creature getPlayer() {
        return creatures.getCreature(playerCreatureId);
    }

    public boolean getShowStats() {
        return showStats;
    }

    public boolean getShowVectors() {
        return showVectors;
    }

    public boolean getMapCollisionsEnabled() {
        return mapCollisionsEnabled;
    }

    public void onEvent(ControlEvent event) {
        switch (event.action) {
            case PAUSE:
                paused = !paused;
                break;
            case TOGGLE_STATS:
                showStats = !showStats;
                break;
            case TOGGLE_VECTORS:
                showVectors = !showVectors;
                break;
            case TOGGLE_COLLISIONS:
                mapCollisionsEnabled = !mapCollisionsEnabled;
                break;
            case CHANGE_CREATURE:
                changeCreature();
                break;
        }
    }

    public void onEvent(GenericGameEvent event) {
        switch (event.eventName) {
            case "setTile":
                setTileUnderPlayer((MapObjectType) event.eventPayload);
                break;
        }
    }

    private void setTileUnderPlayer(MapObjectType type) {
        Creature creature = getPlayer();

        Point mapPoint = GameSizes.getTileCoordinates(creature.body.getPosition());

        gameMap.setTile(mapPoint.x, mapPoint.y, type);
    }

}

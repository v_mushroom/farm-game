/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.farm.game;

/**
 *
 * @author mush
 */
public class GameControlJoystick {
    
    private boolean left;
    private boolean right;
    private boolean up;
    private boolean down;
    
    private int xJoystick = 0;
    private int yJoystick = 0;
    private boolean modified = false;
    
    public int getXJoystick() {
        return xJoystick;
    }
    
    public int getYJoystick() {
        return yJoystick;
    }
    
    public boolean isModified() {
        boolean value = modified;
        modified = false;
        return value;
    }

    public void pushLeft() {
        left = true;
        xJoystick = -1;
        modified = true;
    }

    public void pushRight() {
        right = true;
        xJoystick = 1;
        modified = true;
    }

    public void pushUp() {
        up = true;
        yJoystick = -1;
        modified = true;
    }

    public void pushDown() {
        down = true;
        yJoystick = 1;
        modified = true;
    }

    public void releaseLeft() {
        left = false;
        xJoystick = right ? 1 : 0;
        modified = true;
    }

    public void releaseRight() {
        right = false;
        xJoystick = left ? -1 : 0;
        modified = true;
    }

    public void releaseUp() {
        up = false;
        yJoystick = down ? 1 : 0;
        modified = true;
    }

    public void releaseDown() {
        down = false;
        yJoystick = up ? -1 : 0;
        modified = true;
    }

}

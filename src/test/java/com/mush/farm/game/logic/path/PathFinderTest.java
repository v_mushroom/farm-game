/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.farm.game.logic.path;

import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author mush
 */
public class PathFinderTest {

    public PathFinderTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of find method, of class PathFinder.
     */
    @Test
    public void testFind() {
        Graph graph = new Graph();
        /* hexagon
          a
         / \
        b   c
         \  |
          d |
         /  |
        e   f
         \ /
          g
         */
        int a = graph.addNode(10, 0);
        int b = graph.addNode(0, 10);
        int c = graph.addNode(20, 10);
        int d = graph.addNode(10, 20);
        int e = graph.addNode(0, 30);
        int f = graph.addNode(20, 30);
        int g = graph.addNode(10, 40);

        graph.connect(a, b);
        graph.connect(b, d);
        graph.connect(d, e);
        graph.connect(e, g);

        graph.connect(a, c);
        graph.connect(c, f);
        graph.connect(f, g);

        PathFinder finder = new PathFinder(graph);
        finder.setStartEnd(a, g);
        List<GraphNode> result = finder.find();

        for (GraphNode node : result) {
            System.out.println(node.id + " : " + node.position);
        }
        
        assertEquals(4, result.size());
    }

    /**
     * Test of find method, of class PathFinder.
     */
    @Test
    public void testFindNone() {
        Graph graph = new Graph();
        /* hexagon
          a
         / \
        b   c
         \
          d
         /  
        e   f
           /
          g
         */
        int a = graph.addNode(10, 0);
        int b = graph.addNode(0, 10);
        int c = graph.addNode(20, 10);
        int d = graph.addNode(10, 20);
        int e = graph.addNode(0, 30);
        int f = graph.addNode(20, 30);
        int g = graph.addNode(10, 40);

        graph.connect(a, b);
        graph.connect(b, d);
        graph.connect(d, e);
        //graph.connect(e, g);

        graph.connect(a, c);
        //graph.connect(c, f);
        graph.connect(f, g);

        PathFinder finder = new PathFinder(graph);
        finder.setStartEnd(a, g);
        List<GraphNode> result = finder.find();

        assertEquals(null, result);
    }

}
